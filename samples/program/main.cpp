#include <iostream>

#include <chrono>
#include <thread>

int main(int argc, char* argv[])
{
    std::cout << "Sample Program, logs std out " << std::endl;

    long tick = 1;
    while(true){
        std::cout << "Sample Program tick : " << tick << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        tick++;
    }
}
