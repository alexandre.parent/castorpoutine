require('update-electron-app')({
  logger: require('electron-log')
})

const path = require('path')
const glob = require('glob')
const {app, ipcMain, BrowserWindow, Tray} = require('electron')

const debug = /--debug/.test(process.argv[2])

if (process.mas)
    app.setName('CastorPoutine')

let mainWindow = null

function initialize () {
  makeSingleInstance()

  loadCastorPoutine()

  function createWindow () {
    var windowOptions = {
      width: 1920,
      minWidth: 1440,
      backgroundColor: '#312450',
      height: 1080,
      title: app.getName(),
      icon: path.join(__dirname, 'assets/orig_app-icon/png/256.png'),
      /*webPreferences: {
        nodeIntegration: true
      }*/
    }
    console.error('LOOK AT ME : ' + __dirname)
    //if (process.platform === 'linux') {
      //windowOptions.icon =
     //const appIcon = new Tray(path.join(__dirname, '/assets/3rdparty/linux/linux.png'))
     // windowOptions.icon = path.join(__dirname, '/assets/3rdparty/linux/linux.png')
     windowOptions.icon = path.join(__dirname, '/assets/app-icon/png/128.png')
    //}

    mainWindow = new BrowserWindow(windowOptions)
    mainWindow.maximize()
    mainWindow.loadURL(path.join('file://', __dirname, '/index.html'))
    //mainWindow.openDevTools();

    //=================================================================================
    // CASTORPOUTINE custom section
    //=================================================================================

    // Launch fullscreen with DevTools open, usage: npm run debug
    if (debug) {
      mainWindow.webContents.openDevTools()
      mainWindow.maximize()
      require('devtron').install()
    }

    mainWindow.on('closed', () => {
      mainWindow = null
    })
  }

  app.on('ready', () => {
    createWindow()
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', () => {
    if (mainWindow === null) {
      createWindow()
    }
  })

}

// Make this app a single instance app.
//
// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.
//
// Returns true if the current version of the app should quit instead of
// launching.
function makeSingleInstance () {
  if (process.mas) return

  app.requestSingleInstanceLock()

  app.on('second-instance', () => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })
}

// Require each JS file in the main-process dir
function loadCastorPoutine () {
  const files = glob.sync(path.join(__dirname, 'main-process/**/*.js'))
  files.forEach((file) => { require(file); })
}

//Castor poutine requires some environment variable to work correctly. If not set log and exit.
//This will diminish code needed to get things going.

CASTORPOUTINE_ROOT_FOLDER       = process.env.CASTORPOUTINE_ROOT_FOLDER         //  value : /castorpoutine /var/castorpoutine d:/castorpoutine.
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/conf /var/castorpoutine/temp d:/castorpoutine/conf
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/temp /var/castorpoutine/temp d:/castorpoutine/temp

var bIsEnvironmentCorrectlySetup = true

if(CASTORPOUTINE_ROOT_FOLDER == ''){
    console.error('Error - ENV VAR : CASTORPOUTINE_ROOT_FOLDER is empty')
    bIsEnvironmentCorrectlySetup = false
}
if(CASTORPOUTINE_TEMP_FOLDER_PATH == ''){
    console.error('Error - ENV VAR : CASTORPOUTINE_TEMP_FOLDER_PATH is empty')
    bIsEnvironmentCorrectlySetup = false
}
if(CASTORPOUTINE_TEMP_FOLDER_PATH == ''){
    console.error('Error - ENV VAR : CASTORPOUTINE_TEMP_FOLDER_PATH is empty')
    bIsEnvironmentCorrectlySetup = false
}


if(!bIsEnvironmentCorrectlySetup){
    console.error('Error - Abording CastorPoutine Launch, fix ENV VAR ! ')
    return ;
}

//console.info('--- Creating node runner archive ... + ' + __dirname)

/* Quick packaging of current runner. to add -ef */
//var Zip = require("adm-zip");
//var zip = new Zip();
//zip.addLocalFolder(__dirname + "/../runner");
//zip.extractAllTo(__dirname + "/../runner_tmp", /*overwrite*/true);
//zip.writeZip( __dirname + "/../runner.zip");

initialize()
