const {BrowserWindow, ipcRenderer} = require('electron')
const http = require('http')

var fs = require('fs')

window.$ = window.jQuery = require('jquery')
const jqueryui = require('jquery-ui')
const fancytree = require('jquery.fancytree')
const uuidv1 = require('uuid/v1')


var ConfigurationCtrl = require( './controllers/configurationctrl.js')          // include global services methods
var configurationCtrl = new ConfigurationCtrl()

var NodeDeploymentCtrl = require( './controllers/nodedeploymentctrl.js')        // include global services methods
var nodeDeploymentCtrl = new NodeDeploymentCtrl()

var SshCtrl = require( './controllers/sshctrl.js')          // include global services methods
var sshCtrl = new SshCtrl()

require('jquery.fancytree/dist/modules/jquery.fancytree.filter')
require('jquery.fancytree/dist/modules/jquery.fancytree.glyph')
require('jquery.fancytree/dist/modules/jquery.fancytree.edit')
require('jquery.fancytree/dist/modules/jquery.fancytree.dnd5')
require('jquery.fancytree/dist/modules/jquery.fancytree.table')
require('jquery.fancytree/dist/modules/jquery.fancytree.themeroller')
require('jquery.fancytree/dist/modules/jquery.fancytree.ui-deps')
require('jquery.fancytree/dist/modules/jquery.fancytree.gridnav')


require('./renderer-dialogs.js')

//-- Globals for castorpoutine init phase
count = 0
intervalMethodDelayInMs = 3000
initialIntervalConfiguration = true

const jqueryFancyTreedElement = document.getElementById('tree')
/*
ipcRenderer.on('selected_configuration_ui_event' , (event,  args) => {
    resetJQueryFancyTree()
    updateJQueryFancyTree(args)
    updateConfigurationPropertyTreeUi(args)
})

ipcRenderer.on('select-node' , (event,  args) => {
    console.log('select-node')
})
*/
function startRendererIntervalFunction(){

    var intervalObject = setInterval(function () {

        var tempState = 'OPERATIONAL'
        console.log(count, 'Renderer castorpoutine init Interval Function');
        console.log(count, 'configurationCtrl.isChanged : ' + configurationCtrl.isChanged);
        //looks like .isChanged is not triggered, might be threading related.
        if ( configurationCtrl != null && configurationCtrl != '' && configurationCtrl.isChanged == true ) {
            console.log('Renderer Interval - Config changed : ' + configurationCtrl.getCurrentConfiguration().segment.uuid)

            //Possible to run it async but can be run here since we are already an async task ... ?
            //to be done for runners packaging the http client . ipcRenderer.send('configuration-interval-update', configurationCtrl.getCurrentConfiguration())
            ipcRenderer.send('update-node-status', configurationCtrl.getCurrentConfiguration())
            //ipcRenderer.send('render-configuration-table', configurationCtrl.getCurrentConfiguration())
            // configurationCtrl.saveTempConfiguration() code not rdy yet
        }
        if ( configurationCtrl != null && configurationCtrl != '' && configurationCtrl.getCurrentConfiguration() != '') {
            console.log('Renderer Interval Update')

            configurationCtrl.getCurrentConfiguration().segment.group.node.forEach(function(nodeEntry) {

                //establish http link.
                var sshConnectionForNodeEntry = sshCtrl.getSshSession(nodeEntry.uuid)
                if(sshConnectionForNodeEntry == undefined || sshConnectionForNodeEntry == '' || sshConnectionForNodeEntry.connection == null){
                    establishSshConnection(nodeEntry)
                } else {
                    console.log(nodeEntry._name + ': SSH ONLINE')
                }
                var httpConnectionForNodeEntry = sshCtrl.getHTTPSession(nodeEntry.uuid)
                if(typeof httpConnectionForNodeEntry == 'undefined' || httpConnectionForNodeEntry == ''){
                    establishHTTPConnection(nodeEntry)//so far not needed, as runners are looping us.
                } else {
                    console.log(nodeEntry._name + ': HTTP ONLINE')
                }


                //1. Star package sync task
                var packagesList = new Array();
                visitConfigurationPackagesForSync(nodeEntry, packagesList)
                syncPackagesListSsh(nodeEntry, packagesList)
/*

                var sshSession = sshCtrl.getSshSession(nodeEntry.uuid)
                if(sshSession != ''){


                    var failed = []
                    var successful = []
                    ssh.putDirectory("/home/whoami/work/gitlab/castorpoutine/conf/pkg",
                                     "/home/castorpoutine/conf/NEW_CONF_2019_11_09_2_56_34",
                                     {
                        recursive: true,
                        concurrency: 10,
                        validate: function(itemPath) {
                          const baseName = path.basename(itemPath)
                          return baseName.substr(0, 1) !== '.' && // do not allow dot files
                                 baseName !== 'node_modules' // do not allow node_modules
                        },
                        tick: function(localPath, remotePath, error) {
                          if (error) {
                            failed.push(localPath)
                          } else {
                            successful.push(localPath)
                          }
                        }
                      }).then(function(status) {
                        console.log('the directory transfer was', status ? 'successful' : 'unsuccessful')
                        console.log('failed transfers', failed.join(', '))
                        console.log('successful transfers', successful.join(', '))
                    })
                }*/
            })//end Interval.

            //Possible to run it async but can be run here since we are already an async task ... ?
            ipcRenderer.send('configuration-state-update')
            // configurationCtrl.saveTempConfiguration() code not rdy yet
        }

/*
        sshSession.putFiles([{
            local: process.env.CASTORPOUTINE_CONF_FOLDER_PATH + "/" + ptrCurrentConfiguration.segment._name + "/" + ptrCurrentConfiguration.segment._name + ".json",
            remote: "/home/castorpoutine/conf/"+ ptrCurrentConfiguration.segment._name + ".json" }]).then(function() {
           initialIntervalConfiguration = false
        }, function(error) {
            console.log("Windows ssh error : " + error)
        })
*/
/*
        ssh.execCommand('chmod -R 774 *', { cwd: '/home/castorpoutine'}).then(function(result) {
            console.log('chmod -R 744 STDOUT: ' + result.stdout)
            console.log('chmod -R 744 STDERR: ' + result.stderr)
        })
*/

        count++
    }, intervalMethodDelayInMs)
}

//. helping method
/**/
function visitConfigurationPackagesForSync(aNode, packagesInfo){

    if(aNode.package != undefined){
        aNode.package.forEach(function(pkg) {
            packagesInfo.push(pkg)
        })
    }

    aNode.packageGroup.forEach(function(pkg) {
        if(pkg.fancytreeNodeType == 'packagegroup'){
            visitConfigurationPackagesForSync(pkg, packagesInfo)
        }
    })
}

function syncPackagesList(nodeEntry, packagesList){

    packagesList.forEach(function(pkg) {
        //fancytreeNodeType
        // make http available. so config on the other side can just download it.
    })
}

//... Ask main program for our data.
ipcRenderer.send('read-configurations', null)

//..Ask for system tray
ipcRenderer.send('put-in-tray', configurationCtrl.getCurrentConfigurationName())

//.Display panels
window.$("#tree_selection_container_packagegroup").hide()
window.$("#tree_selection_container_package").hide()
window.$("#tree_selection_container_node").hide()
window.$("#tree_selection_container_configuration").show()

startRendererIntervalFunction()

function establishSshConnection(pNodeEntry){

    if(pNodeEntry.username == '' || pNodeEntry.username == undefined || pNodeEntry.sshpwd == '' || pNodeEntry.sshpwd == undefined){
        console.log('SSH no user or pass for node : ' + pNodeEntry._name)
        return
    }

    node_ssh = require('node-ssh')
    ssh = new node_ssh()

    ssh.connect({
        host: pNodeEntry.ip,
        username: pNodeEntry.username,
        password: pNodeEntry.sshpwd,
        //privateKey: '/home/castorpoutine/.ssh/id_rsa',
        port: 22,
        tryKeyboard: true,
        onKeyboardInteractive: (name, instructions, instructionsLang, prompts, finish) => {
          if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
            finish([password])
          }
        }
    })
    .then(function() {

      if(ssh.connection == null)
      {
          console.error('SSH Connection fail for : pNodeEntry.ip ' + pNodeEntry.ip)
          console.error('SSH Connection fail for : username ' + username)
          console.error('SSH Connection fail for : password ' + password)

          return ;
      }

      console.log('SSH connected for Node : ' + pNodeEntry._name + " - " + pNodeEntry.ip )

      var ptrCurrentConfiguration = configurationCtrl.getCurrentConfiguration()

      //check if debug mode, do not start runners. start manually to debug its code easier.
/**/
      if(pNodeEntry.os == 'Linux') {

            ssh.execCommand('rm -f runner.zip', { remote: '/home/castorpoutine'}).then(function(result) {
                console.log('rm -f runner.zip STDOUT: ' + result.stdout)
                console.log('rm -f runner.zip STDERR: ' + result.stderr)
                pNodeEntry.httpStatus = 'Deleting runner.zip'
                ssh.execCommand('rm -rf runner', { remote: '/home/castorpoutine'}).then(function(result) {
                    console.log('rm -rf runner STDOUT: ' + result.stdout)
                    console.log('rm -rf runner STDERR: ' + result.stderr)
                    pNodeEntry.httpStatus = 'Deleting runner'
                    ssh.putFiles([{ local: process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC, remote: '/home/castorpoutine/runner.zip' }]).then(function() {
                        console.log('putFiles process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC: ' + result.stdout)
                        console.log('putFiles process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC: ' + result.stderr)
                        pNodeEntry.httpStatus = 'Uploading runner.zip'
                       ssh.execCommand('mkdir runner', { remote: '/home/castorpoutine'}).then(function(result) {
                           console.log('mkdir runner ' + result.stdout)
                           console.log('mkdir runner ' + result.stderr)
                           pNodeEntry.httpStatus = 'Creating runner'
                           ssh.execCommand('unzip /home/castorpoutine/runner.zip -d /home/castorpoutine', { remote: '/home/castorpoutine'}).then(function(result) {
                               console.log('unzip /home/castorpoutine/runner.zip -d /home/castorpoutine' + result.stdout)
                               console.log('unzip /home/castorpoutine/runner.zip -d /home/castorpoutine' + result.stderr)
                               pNodeEntry.httpStatus = 'Expanding runner'
/*
                               ssh.execCommand('chmod 744 /home/castorpoutine/runner/linux_castorpoutine_runner.sh', { remote: '/home/castorpoutine/runner'}).then(function(result) {
                                   ssh.execCommand('/home/castorpoutine/runner/linux_castorpoutine_runner.sh ' + pNodeEntry._name +' '+ pNodeEntry.ip + ' ' + pNodeEntry.port + ' ' + process.env.CASTORPOUTINE_IP +' '+ process.env.CASTORPOUTINE_PORT , { remote: '/home/castorpoutine/runner'}).then(function(result) {
                                     console.log('linux_castorpoutine_runner STDOUT: ' + result.stdout)
                                     console.log('linux_castorpoutine_runner STDERR: ' + result.stderr)
                                     pNodeEntry.httpStatus = 'Running'
                                   })
                               })
                               */
                           })
                       })
                    }, function(error) {
                        console.log("Something's wrong linux ssh : " + error)
                        })
                })
            })
        //})

          ssh.putFiles([{
              local: process.env.CASTORPOUTINE_CONF_FOLDER_PATH + "/" + ptrCurrentConfiguration.segment._name + "/" + ptrCurrentConfiguration.segment._name + ".json",
              remote: "/home/castorpoutine/conf/"+ ptrCurrentConfiguration.segment._name + ".json" }]).then(function() {
             initialIntervalConfiguration = false
          }, function(error) {
              console.log("Windows ssh error : " + error)
          })

          ssh.execCommand('chmod -R 774 *', { cwd: '/home/castorpoutine'}).then(function(result) {
              console.log('chmod -R 744 STDOUT: ' + result.stdout)
              console.log('chmod -R 744 STDERR: ' + result.stderr)
          })

          //pNodeEntry.sshConnection = ssh
          pNodeEntry.sshStatus = 'SSH : OK'
          pNodeEntry.isChanged = true
          //ipcRenderer.send('SSH_NODE_STATUS', null)
          //ipcRenderer.send('reset-fancytree', null)
          //pNodeEntry.uuid = uuidv1()
          //ipcRenderer.send('update-node-status', pNodeEntry.uuid)
          currentConfiguration.uuid = uuidv1()
          sshCtrl.addSshSession(pNodeEntry.uuid, ssh)

      } else if (pNodeEntry.os == 'Windows') {

        console.log("pNodeEntry.os == Windows")

          // Command
          var ptrCurrentConfiguration = configurationCtrl.getCurrentConfiguration()

            //Create standard folder structure for castorpoutine software. process.env.CASTORPOUTINE_TEMP_FOLDER_PATH
            ssh.execCommand('taskkill /F /FI "USERNAME eq castorpoutine" /IM node.exe', { remote: 'C:/Users/castorpoutine'}).then(function(result) {
                console.log('del /f runner.zip STDOUT: ' + result.stdout)
                console.log('del /f runner.zip STDERR: ' + result.stderr)
                ssh.execCommand('del /f runner.zip', { remote: 'C:/Users/castorpoutine'}).then(function(result) {
                    console.log('del /f runner.zip STDOUT: ' + result.stdout)
                    console.log('del /f runner.zip STDERR: ' + result.stderr)
                    pNodeEntry.httpStatus = 'Deleting runner.zip'
                    ssh.execCommand('rmdir /S /Q runner', { remote: 'c:/Users/castorpoutine'}).then(function(result) {
                        console.log('rmdir /S /Q runner STDOUT: ' + result.stdout)
                        console.log('rmdir /S /Q runner STDERR: ' + result.stderr)
                        pNodeEntry.httpStatus = 'Deleting runner'
                        ssh.putFiles([{ local: process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC, remote: 'c:/Users/castorpoutine/runner.zip' }]).then(function() {
                            console.log('putFiles process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC: ' + result.stdout)
                            console.log('putFiles process.env.CASTORPOUTINE_DAEMON_RUNNER_SRC: ' + result.stderr)
                            pNodeEntry.httpStatus = 'Uploading runner.zip'
                           ssh.execCommand('mkdir runner', { remote: 'c:/Users/castorpoutine'}).then(function(result) {
                               console.log('mkdir runner ' + result.stdout)
                               console.log('mkdir runner ' + result.stderr)
                               pNodeEntry.httpStatus = 'Creating runner'
                               ssh.execCommand('powershell Expand-Archive -path "runner.zip" –DestinationPath "."', { remote: 'C:/Users/castorpoutine'}).then(function(result) {
                                   console.log('powershell Expand-Archive -path "' + result.stdout)
                                   console.log('powershell Expand-Archive -path "' + result.stderr)
                                   pNodeEntry.httpStatus = 'Expanding runner'
                                   ssh.execCommand('c:/Users/castorpoutine/runner/windows_castorpoutine_runner.bat ' + pNodeEntry._name +' '+ pNodeEntry.ip + ' ' + pNodeEntry.port + ' ' + process.env.CASTORPOUTINE_IP +' '+ process.env.CASTORPOUTINE_PORT , { remote: 'c:/Users/castorpoutine/runner'}).then(function(result) {
                                     console.log('windows_castorpoutine_runner STDOUT: ' + result.stdout)
                                     console.log('windows_castorpoutine_runner STDERR: ' + result.stderr)
                                     pNodeEntry.httpStatus = 'Running'
                                   })
                               })
                           })
                    }, function(error) {
                      console.log("Something's wrong windows ssh : " + error)
                    })
                  })
                })
            })

            ssh.putFiles([{ local: process.env.CASTORPOUTINE_CONF_FOLDER_PATH + "/" + ptrCurrentConfiguration.segment._name + "/" + ptrCurrentConfiguration.segment._name + ".json", remote: 'C:/Users/castorpoutine/conf'+ "/" + ptrCurrentConfiguration.segment._name + ".json" }]).then(function() {
             initialIntervalConfiguration = false
            }, function(error) {
            console.log("Windows ssh error : " + error)
            //console.log(error)
            })

            ssh.putFiles([{ local: process.env.CASTORPOUTINE_NODEJS_WIN64_MSI, remote: 'c:/Users/castorpoutine/node-v12.10.0-x64.msi' }]).then(function() {
             initialIntervalConfiguration = false
             ssh.execCommand('powershell Expand-Archive -path "./runner.zip" –DestinationPath "./runner"').then(function(result) {
                 if(result.code != 0){
                   console.log('STDOUT: ' + result.stdout)
                   console.log('STDERR: ' + result.stderr)
               }
             })
            }, function(error) {
            console.log("Windows ssh error : " + error)
            //console.log(error)
            })

            //pNodeEntry.sshConnection = ssh
            pNodeEntry.sshStatus = 'SSH : OK'
            pNodeEntry.isChanged = true
            //pNodeEntry.uuid = uuidv1()
            currentConfiguration.uuid = uuidv1()
            //ipcRenderer.send('update-node-status', pNodeEntry.uuid)
            sshCtrl.addSshSession(pNodeEntry.uuid, ssh)
        }

    configurationCtrl.isChanged = true
    })//end then
}

function establishHTTPConnection(pNodeEntry){

    //console.log('Entering : establishHTTPConnection')

         //host: pNodeEntry.ip,
         //username: 'castorpoutine',
         //port: pNodeEntry.httpPort
         //password: pNodeEntry.sshpwd,

    //pNodeEntry.httpStatus = 'HTTP : OFFLINE'
}

var express               = require('express')
var routes                = require('./castorpoutine-routes.js')

const path = require('path');

var expressServerInstance = express()


function castorpoutineExpress(){

    var server = expressServerInstance.listen(8080, function () {

        // Parameters
        // ====================================================================
        var host = server.address().address
        var port = server.address().port

        console.log("castorpoutine electron express - listening at http://%s:%s", host, port)

        // Route expressServerInstance.
        // ====================================================================
        routes.initialize(expressServerInstance, ipcRenderer)

        // Service communications
        // ====================================================================
    })
}

function syncPackagesListSsh(aNode, packageList){
    console.log('syncPackagesListSsh node : ' + aNode._name)
    ///
    sshSession = sshCtrl.getSshSession(aNode.uuid)

    if ( sshSession != undefined && sshSession != 'undefined' && sshSession != '' ) {
        console.log('syncPackagesListSsh sshSession != undefined')

        packageList.forEach(function(pkg) {

            if ( pkg._name != ''  && pkg._name != undefined) {
                console.log('syncPackagesListSsh pkg._name' + pkg._name)

                // Putting entire directories
                const failed = []
                const successful = []

                sshSession.execCommand('mkdir -p '+'/home/castorpoutine/' + path.join(pkg._location), { cwd:'/home/castorpoutine' }).then(function(result) {
                   console.log('STDOUT: ' + result.stdout)
                   console.log('STDERR: ' + result.stderr)
                 })

                sshSession.putDirectory(process.env.CASTORPOUTINE_CONF_FOLDER_PATH + '/' + path.join(pkg._location), '/home/castorpoutine/' + path.join(pkg._location), {
                    recursive: true,
                    concurrency: 10,
                    validate: function(itemPath) {
                      const baseName = path.basename(itemPath)
                      return baseName.substr(0, 1) !== '.' && // do not allow dot files
                             baseName !== 'node_modules' // do not allow node_modules
                    },
                    tick: function(localPath, remotePath, error) {
                      if (error) {
                        failed.push(localPath)
                      } else {
                        successful.push(localPath)
                      }
                    }
                    }).then(function(status) {
                    console.log('the directory transfer was', status ? 'successful' : 'unsuccessful')
                    console.log('failed transfers', failed.join(', '))
                    console.log('successful transfers', successful.join(', '))
                })
            }
        })
    }
}

castorpoutineExpress()
