//routes.js
//1. features
//1.1 map url to specific method calls for futher usage
// ====================================================================
//var express = require('express');

//require('./services.js');    // include global services methods
//var WebServices = require('./services.js');    // include global services methods
//var webServices = new WebServices();
bodyParser = require('body-parser');

module.exports = {
    initialize: function ( expressServerInstance, ipcMain ) {
        initialize(expressServerInstance, ipcMain);
    }
}

//todo a.p will prob wont need a listener http srv.
// Private methods
// ====================================================================
var initialize = function ( expressServerInstance, ipcRenderer ) {
    console.log('castorpoutine-routes initialize !')

    expressServerInstance.use( bodyParser.json() )

    //expressServerInstance.use('/static', expressServerInstance.static(__dirname))


    expressServerInstance.get('/', function(req, res) {
        //res.sendfile('index.html', { root: __dirname + "/../ui" } );
        console.log('castorpoutine expressServerInstance express : /');
        var returnValue = "this should display frontend webpage.";

        console.log('returnValue : ' + returnValue)
        res.end( returnValue )
    })

    expressServerInstance.post('/node/status', function (req, res) {
        console.log('/node/status')
        console.log(req.body)
        ipcRenderer.send('REST_NODE_STATUS', req.body)
    })
}
