//INCLUDES
const {ipcRenderer} = require('electron')
//const pidusage = require('pidusage')
//const request = require('request')
const moment = require('moment')
const uuidv1 = require('uuid/v1')
const jqueryui = require('jquery-ui')
const fancytree = require('jquery.fancytree')

//******************************************************************************
//** SETUP FOLDERS LOGIC FOR GIVEN RUN
fs = require('fs')
fsextra = require("fs-extra");
os = require('os')

CASTORPOUTINE_ROOT_FOLDER       = process.env.CASTORPOUTINE_ROOT_FOLDER         //  value : /castorpoutine /var/castorpoutine d:/castorpoutine.
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/conf /var/castorpoutine/temp d:/castorpoutine/conf
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/temp /var/castorpoutine/temp d:/castorpoutine/temp


classname = 'NodeDeploymentCtrl'
module.exports = class NodeDeploymentCtrl {

    constructor () {
        console.log('Initializing CLASS : ' + classname)

        var source = '/home/whoami/work/gitlab/castorpoutine/src'
        var destination = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH + '/slave-daemon-1.0.0.0'

        console.log('NodeDeploymentCtrl , source and destination : ' + source + ' - ' + destination)

        // copy source folder to destination
        //Error: Source and destination must not be the same. ---- > already exist but works.
        /*
        fsextra.copy(source, destination, function (err) {
            if (err){
                console.log('An error occured while copying the folder.')
                return console.error(err)
            }
            console.log('Copy completed!')
        });

        var zipFolder = require('zip-folder');

        zipFolder(destination, '/castorpoutine/conf/slave-daemon-1.0.0.0.zip', function(err) {
            if(err) {
                console.log('oh no!', err);
            } else {
                console.log('EXCELLENT');
            }
        });
        */
        ipcRenderer.on('configuration-generate-archives' , (event, configuration) => {

            if(configuration == '')
             return

            console.log('NodeDeploymentCtrl : !!!_configuration-generate-archives_!!!')
            console.log(configuration.segment._name)
            var stringValue = ''

            //1. Copy runner to temp folder.

            //2. Tune specific runner folder //generate conf files. args conf_name.node_name.json
            configuration.segment.group.node.forEach(function(nodeEntry) {

                //stringValue = nodeEntry._title + ',' + nodeEntry._name + ',' + nodeEntry.ip + ',' + nodeEntry.port + ',' + nodeEntry.settings + ',' + nodeEntry.env + ',' + nodeEntry.performance
                //console.log(stringValue)

                var jsonNodeDaemonConfiguration = {}
                jsonNodeDaemonConfiguration.name = nodeEntry._name
                jsonNodeDaemonConfiguration.masterip = process.env.CASTORPOUTINE_IP
                jsonNodeDaemonConfiguration.masterport = process.env.CASTORPOUTINE_PORT
                jsonNodeDaemonConfiguration.http = {}
                jsonNodeDaemonConfiguration.http.url = nodeEntry.ip
                jsonNodeDaemonConfiguration.http.port = nodeEntry.port
                jsonNodeDaemonConfiguration.masterip = process.env.CASTORPOUTINE_IP
                jsonNodeDaemonConfiguration.masterport = process.env.CASTORPOUTINE_PORT
                jsonNodeDaemonConfiguration.generated = 'true'

                var stringifyedConf = JSON.stringify(jsonNodeDaemonConfiguration)

                //check if temp folder exist.
                var folderPath = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH
                var nodeRunnerConfigurationFilename = configuration.segment._name + '_' + nodeEntry._name + '.json'
                var fullPathToJson = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH +'/'+ nodeRunnerConfigurationFilename
                var folderExist = fs.existsSync( folderPath )

                // Check if the file exists in the current directory, and if it is writable.
                fs.access(folderPath, fs.constants.F_OK | fs.constants.W_OK, (err) => {
                  if (err) {
                    fs.mkdir( folderPath ,  (err) => {
                        if (err) throw err
                        console.log('mkdir configuration : ' + folderPath)
                    })
                  }
                })

                fs.open(fullPathToJson, 'wx', (err, fd) => {
                    if (err) {
                        if (err.code === 'EEXIST') {
                            console.error('myfile already exists');
                            fs.renameSync(fullPathToJson, fullPathToJson+".prev")
                        }
                    }

                    fs.writeFile( fullPathToJson, stringifyedConf, (err) => {
                        if (err) throw err
                            console.log('The config currentConfigurationName has been saved!')
                    })
                })
            })

            //3. send system commands to create the archives.
            // destination.txt will be created or overwritten by default.
        })
    }
}

function packageNodeRunnerDeploymentPayload(){

}
