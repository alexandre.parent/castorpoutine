/******************************************************************************

Class       : ConfigurationCtrl
Job         : All CastorPoutine configuration related action
Function    : CRUD /castorpoutine/conf

*******************************************************************************/

//INCLUDES
const {ipcRenderer} = require('electron')
//const pidusage = require('pidusage')
const request = require('request')
const moment = require('moment')
const uuidv1 = require('uuid/v1')
const jqueryui = require('jquery-ui')
const fancytree = require('jquery.fancytree')
beautify = require('js-beautify')

window.$ = window.jQuery = require('jquery')

tree = ''


//******************************************************************************
//** SETUP FOLDERS LOGIC FOR GIVEN RUN
fs = require('fs')
os = require('os')

//All ENV VAR needed for this controller listed here. If one is not found log it as fatal error.
//Reason for this simplistic approach simpler=better.
//Cannot be null since check at program launch.!.;
CASTORPOUTINE_ROOT_FOLDER       = process.env.CASTORPOUTINE_ROOT_FOLDER         //  value : /home/castorpoutine /var/castorpoutine d:/castorpoutine.
CASTORPOUTINE_CONF_FOLDER_PATH  = process.env.CASTORPOUTINE_CONF_FOLDER_PATH    //  value : /home/castorpoutine/conf /var/castorpoutine/temp d:/castorpoutine/conf
CASTORPOUTINE_PKG_FOLDER_PATH   = process.env.CASTORPOUTINE_PKG_FOLDER_PATH
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/temp /var/castorpoutine/temp d:/castorpoutine/temp

//******************************************************************************
//HTML object references
const configurationSelectBtn              = document.getElementById('configuration_select');configurationSelectBtnPreviousValue       = '';
const configurationState                  = document.getElementById('configuration_state')
const configurationLoad                   = document.getElementById('configuration_load')
const configurationUnload                 = document.getElementById('configuration_unload')
const configurationCreateBtn              = document.getElementById('configuration_create')
const configurationCreateResponseBtn      = document.getElementById('configuration_create_response')
const configurationCopyBtn                = document.getElementById('configuration_copy')
const configurationCopyResponseBtn        = document.getElementById('configuration_copy_response')
const configurationExportBtn              = document.getElementById('configuration_export')
const configurationExportResponseBtn      = document.getElementById('configuration_export_response')
const configurationImportBtn              = document.getElementById('configuration_import')
const configurationImportResponseBtn      = document.getElementById('configuration_import_response')
const configurationDeleteBtn              = document.getElementById('configuration_delete')
const configurationDeleteResponseBtn      = document.getElementById('configuration_delete_response')

//Configuration property HTML elements
const configurationEditNameBtn            = document.getElementById('configuration_edit_name')
const configurationEditWorkingFolderBtn   = document.getElementById('configuration_edit_workingfolder')
const configurationEditLoadSequenceBtn    = document.getElementById('configuration_edit_loadsequence')
const configurationEditGrpSetBt = document.getElementById('configuration_edit_groupsettings')
const configurationEditInstallPathBtn     = document.getElementById('configuration_edit_installpath')
configurationEditNameBtnPreviousValue               = ''
configurationEditWorkingFolderBtnPreviousValue      = ''
configurationEditLoadSequenceBtnPreviousValue       = ''
configurationEditGrpSetBtPreviousValue    = ''
configurationEditInstallPathBtnPreviousValue        = ''

const nodeEditNameBtn                     = document.getElementById('node_edit_name')
const nodeEditUsernameBtn                 = document.getElementById('node_edit_username')
const nodeEditIpBtn                       = document.getElementById('node_edit_ip')
const nodeEditOsBtn                       = document.getElementById('node_edit_os')
const nodeEditSshkeyBtn                   = document.getElementById('node_edit_sshkey')
const nodeEditSshpwdBtn                   = document.getElementById('node_edit_sshpwd')
const nodeEditPortBtn                     = document.getElementById('node_edit_port')
const nodeEditSettingsBtn                 = document.getElementById('node_edit_settings')
const nodeEditEnvBtn                      = document.getElementById('node_edit_env')
const nodeEditPerformanceBtn              = document.getElementById('node_edit_performance')
nodeEditNameBtnPreviousValue              = ''
nodeEditUsernameBtnPreviousValue          = ''
nodeEditIpBtnPreviousValue                = ''
nodeEditOsBtnPreviousValue                = ''
nodeEditSshkeyBtnPreviousValue            = ''
nodeEditSshpwdBtnPreviousValue            = ''
nodeEditPortBtnPreviousValue              = ''
nodeEditSettingsBtnPreviousValue          = ''
nodeEditEnvBtnPreviousValue               = ''
nodeEditPerformanceBtnPreviousValue       = ''

const packagegroupEditNameBtn             = document.getElementById('packagegroup_edit_name')
packagegroupEditNameBtnPreviousValue      = ''

const packageEditNameBtn                  = document.getElementById('package_edit_name')
const packageEditAliasBtn                 = document.getElementById('package_edit_alias')
const packageEditLocationBtn              = document.getElementById('package_edit_location')
const packageEditInstallPathBtn           = document.getElementById('package_edit_installpath')
const packageEditXmlLayerFileBtn          = document.getElementById('package_edit_xmlLayerFile')
const packageEditInstallSequenceBtn       = document.getElementById('package_edit_installsequence')
const packageEditLoadSequenceBtn          = document.getElementById('package_edit_loadsequence')
const packageEditSettingsBtn              = document.getElementById('package_edit_settings')
const packageEditCmInfoBtn                = document.getElementById('package_edit_cminfo')
packageEditNameBtnPreviousValue           = ''
packageEditAliasBtnPreviousValue          = ''
packageEditLocationBtnPreviousValue       = ''
packageEditInstallPathBtnPreviousValue    = ''
packageEditXmlLayerFileBtnPreviousValue   = ''
packageEditInstallSequenceBtnPreviousValue= ''
packageEditLoadSequenceBtnPreviousValue   = ''
packageEditSettingsBtnPreviousValue       = ''
packageEditCmInfoBtnPreviousValue         = ''

//CLASS public members
processes = new Array(200) //callback pointers to all processing launched by this weblaunch node
packages = new Array(1000)
configurations = new Array()

currentConfiguration                = ''       // Pointer to current selected  configuration, handy
currentConfigurationName            = ''       // Pointer to current selected  configuration NAME, handy
currentSelectedObj                  = ''
configurationPreviousEditUuid       = ''       // UUID that change every EDIT, interval save triggers every n second
configurationEditCount              = 1
newNodeCount                        = 1
newNodeRunnerPrefix                 = 'NEW_RUNNER_'
newConfigurationPrefix              = 'NEW_CONF_'
newConfigurationCount               = 100
newPackageGroupCount                = 1
newPackageCount                     = 1
currentNode                         = ''
currentPackage                      = ''
currentPackageGroup                 = ''

 // 0 = standby, 1 = conf selected, 2 = synching, 3 = synched, 4 = loading, 5 = running, 6 = unloading
const standby = 0
const conf_selected = 1
const synching = 2
const synched = 3
const loading=4
const running=5
const unloading=6

previousNodeSelected = ''

clickcount = 0//tree helper

classname = 'ConfigurationCtrl'
module.exports = class ConfigurationCtrl {

    getCurrentConfigurationName(){
        return getCurrentConfigurationName()
    }

    getCurrentConfiguration(){
        return currentConfiguration
    }

    //1. Declare all HTML element listeners
    initEventListeners(){

        console.log('initEventListeners')
        //1. Trigger HTML events
        configurationSelectBtn.addEventListener('change', (event) => {
            console.log('configurationSelectBtn : ' + configurationSelectBtn.value)

            if (configurationSelectBtn.value == 'none') {
                ipcRenderer.send('reset-configuration')
            }

            if (configurationSelectBtn.value != '' && configurationSelectBtnPreviousValue != configurationSelectBtn.value) {

                configurations.forEach( function(configurationEntry) {
                    if ( configurationEntry.segment._name == configurationSelectBtn.value ) {
                        currentConfigurationName = configurationEntry.segment._name
                        currentConfiguration = configurationEntry
                        ipcRenderer.send('select-configuration', configurationEntry.segment._name)
                        return ;
                    }
                })
            }
        })

        configurationLoad.addEventListener('click', (event) => {

            if( currentConfiguration != null && currentConfiguration.segment._name != '')
            {
                currentConfiguration.state = loading
                ipcRenderer.send('configuration-state-update')
                ipcRenderer.send('configuration-load', currentConfiguration.segment._name)
            }
        })

        configurationUnload.addEventListener('click', (event) => {

            if( currentConfiguration != null && currentConfiguration.segment._name != '')
            {
                currentConfiguration.state = unloading
                ipcRenderer.send('configuration-state-update')
                ipcRenderer.send('configuration-unload', currentConfiguration.segment._name)
            }
        })

        ipcRenderer.on('configuration-load' , (event,  args) => {
            console.log(' ipc renderer : configuration-load : ' + currentConfiguration.segment._name)

            if(currentConfiguration != '') {

                currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
                    //const formData = JSON.stringify(currentConfiguration)
                    const formData = currentConfiguration
                    var urlTemp = 'http://' + nodeEntry.ip + ':' + nodeEntry.port + '/configuration/load'
                    request.post({
                       url: 'http://' + nodeEntry.ip + ':' + nodeEntry.port + '/configuration/load',
                       json: formData
                     },
                    function (err, httpResponse, body) {
                           console.log(err, body)
                           console.log('error:', err) // Print the error if one occurred
                           console.log('statusCode:', httpResponse && httpResponse.statusCode) // Print the response status code if a response was received
                           console.log('body:', body) // Print the HTML for the Google homepage.
                     })
                })
            } else {
                console.log('Warning - select-package : No Current CONF Pointer !!!')
            }
        })

        configurationUnload.addEventListener('change', (event) => {
            ipcRenderer.send('configuration-unload', configurationEntry.segment._name)
        })


        //input to each key or action in the div.
        //blur to each time outside click
        //change does not work ?
        //Node related HTML buttons
        packagegroupEditNameBtn.addEventListener('blur', (event, args) => {
            console.log('packagegroupEditNameBtn New Value : ' + window.$("#packagegroup_edit_name").html() )
            console.log('packagegroupEditNameBtn Old Value: ' + currentPackageGroup._name)

            if( window.$("#packagegroup_edit_name").html() == packagegroupEditNameBtnPreviousValue)
                return
            if( currentPackageGroup == '')
              console.log('VERY BAD CURRENT PKG GROP EMPTY POINTER !!!')

            currentPackageGroup._name = window.$("#packagegroup_edit_name").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationPreviousEditUuid++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        packageEditNameBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditNameBtn New Value : ' + window.$("#package_edit_name").html() )

            if(currentPackage == ''){
                console.log('VERY BAD CURRENT PKG EMPTY POINTER !!!')
                return
            }

            console.log('packageEditNameBtn Old Value: ' + currentPackage._name)

            if( window.$("#package_edit_name").html() == packageEditNameBtnPreviousValue)
                return

            currentPackage._name = window.$("#package_edit_name").html()
            currentPackage._title = window.$("#package_edit_name").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        packageEditAliasBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditAliasBtn New Value : ' + window.$("#package_edit_alias").html() )
            console.log('packageEditAliasBtn Old Value: ' + currentPackage._alias)

            if( window.$("#package_edit_alias").html() == packageEditAliasBtnPreviousValue)
                return

            currentPackage._alias = window.$("#package_edit_alias").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })

        packageEditLocationBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditLocationBtn New Value : ' + window.$("#package_edit_location").html() )
            console.log('currentpackage value : ' + currentPackage)

            if( window.$("#package_edit_location").html() == packageEditLocationBtnPreviousValue)
                return

            currentPackage._location = window.$("#package_edit_location").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        packageEditXmlLayerFileBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditXmlLayerFileBtn New Value : ' + window.$("#package_edit_xmlLayerFile").html() )
            console.log('packageEditXmlLayerFileBtn Old Value: ' + currentPackage._xmlLayerFile)

            if( window.$("#package_edit_xmlLayerFile").html() == packageEditXmlLayerFileBtnPreviousValue)
                return

            currentPackage._xmlLayerFile = window.$("#package_edit_xmlLayerFile").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })

        packageEditSettingsBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditSettingsBtn New Value : ' + window.$("#package_edit_settings").html() )
            console.log('packageEditSettingsBtn Old Value: ' + currentPackage._settings)

            if( window.$("#package_edit_settings").html() == packageEditSettingsBtnPreviousValue)
                return

            currentPackage._settings = window.$("#package_edit_settings").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })

        packageEditCmInfoBtn.addEventListener('blur', (event, args) => {
            console.log('packageEditCmInfoBtn New Value : ' + window.$("#package_edit_cminfo").html() )
            console.log('packageEditCmInfoBtn Old Value: ' + currentPackage._cmInfo)

            if( window.$("#package_edit_cminfo").html() == packageEditCmInfoBtnPreviousValue)
                return

            currentPackage._cmInfo = window.$("#package_edit_cminfo").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })

        nodeEditNameBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditNameBtn New Value : ' + window.$("#node_edit_name").html() )
            console.log('nodeEditNameBtn Old Value: ' + currentNode._name)

            if( window.$("#node_edit_name").html() == nodeEditNameBtnPreviousValue)
                return

            currentNode._name = window.$("#node_edit_name").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditUsernameBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditUsernameBtn New Value : ' + window.$("#node_edit_username").html() )
            console.log('nodeEditUsernameBtn Old Value: ' + currentNode.username)

            if( window.$("#node_edit_username").html() == nodeEditUsernameBtnPreviousValue)
                return

            currentNode.username = window.$("#node_edit_username").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditIpBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditIpBtn New Value : ' + window.$("#node_edit_ip").html() )
            console.log('nodeEditIpBtn Old Value: ' + currentNode.ip)

            if( window.$("#node_edit_ip").html() == nodeEditIpBtnPreviousValue)
                return

            currentNode.ip = window.$("#node_edit_ip").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditOsBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditOsBtn New Value : ' + window.$("#node_edit_os").html() )
            console.log('nodeEditOsBtn Old Value: ' + currentNode.os)

            if( window.$("#node_edit_os").html() == nodeEditOsBtnPreviousValue)
                return

            currentNode.os = window.$("#node_edit_os").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditSshkeyBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditSshkeyBtn New Value : ' + window.$("#node_edit_sshkey").html() )
            console.log('nodeEditSshkeyBtn Old Value: ' + currentNode.sshkey)

            if( window.$("#node_edit_sshkey").html() == nodeEditSshkeyBtnPreviousValue)
                return

            currentNode.sshkey = window.$("#node_edit_sshkey").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditSshpwdBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditSshpwdBtn New Value : ' + window.$("#node_edit_sshpwd").html() )
            console.log('nodeEditSshpwdBtn Old Value: ' + currentNode.sshkey)

            if( window.$("#node_edit_sshpwd").html() == nodeEditSshpwdBtnPreviousValue)
                return

            currentNode.sshpwd = window.$("#node_edit_sshpwd").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditPortBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditPortBtn New Value : ' + window.$("#node_edit_port").html() )
            console.log('nodeEditPortBtn Old Value: ' + currentNode.port)

            if( window.$("#node_edit_port").html() == nodeEditPortBtnPreviousValue)
                return

            currentNode.port = window.$("#node_edit_port").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditSettingsBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditSettingsBtn New Value : ' + window.$("#node_edit_settings").html() )
            console.log('nodeEditSettingsBtn Old Value: ' + currentNode.settings)

            if( window.$("#node_edit_settings").html() == nodeEditSettingsBtnPreviousValue)
                return

            currentNode.settings = window.$("#node_edit_settings").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditEnvBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditEnvBtn New Value : ' + window.$("#node_edit_env").html() )
            console.log('nodeEditEnvBtn Old Value: ' + currentNode.env)

            if( window.$("#node_edit_env").html() == nodeEditEnvBtnPreviousValue)
                return

            currentNode.env = window.$("#node_edit_env").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })
        nodeEditPerformanceBtn.addEventListener('blur', (event, args) => {
            console.log('nodeEditPerformanceBtn New Value : ' + window.$("#node_edit_performance").html() )
            console.log('nodeEditPerformanceBtn Old Value: ' + currentNode.performance)

            if( window.$("#node_edit_performance").html() == nodeEditPerformanceBtnPreviousValue)
                return

            currentNode.performance = window.$("#node_edit_performance").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('reset-fancytree', '')
        })

        //Configuration related HTML buttons
        configurationEditLoadSequenceBtn.addEventListener('blur', (event, args) => {
            console.log('configurationEditLoadSequenceBtn New Value : ' + window.$("#configuration_edit_loadsequence").html() )
            console.log('configurationEditLoadSequenceBtn Old Value: ' + currentConfiguration.segment._loadsequence)

            if( window.$("#configuration_edit_loadsequence").html() == configurationEditLoadSequenceBtnPreviousValue)
                return

            currentConfiguration.segment.group._loadsequence = window.$("#configuration_edit_loadsequence").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
        })

        configurationEditWorkingFolderBtn.addEventListener('blur', (event, args) => {
            console.log('configurationEditWorkingFolderBtn New Value : ' + window.$("#configuration_edit_workingfolder").html() )
            console.log('configurationEditWorkingFolderBtn Old Value: ' + currentConfiguration.segment._workingFolder)

            if( window.$("#configuration_edit_workingfolder").html() == configurationEditWorkingFolderBtnPreviousValue)
                return

            currentConfiguration.segment.group._workingFolder = window.$("#configuration_edit_workingfolder").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
        })

        configurationEditGrpSetBt.addEventListener('blur', (event, args) => {
            console.log('configurationEditGrpSetBt New Value : ' + window.$("#configuration_edit_groupsettings").html() )
            console.log('configurationEditGrpSetBt Old Value: ' + currentConfiguration.segment._groupsettings)

            if( window.$("#configuration_edit_groupsettings").html() == configurationEditGrpSetBtPreviousValue)
                return

            currentConfiguration.segment.group._groupsettings = window.$("#configuration_edit_groupsettings").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
        })
        configurationEditInstallPathBtn.addEventListener('blur', (event, args) => {
            console.log('configurationEditInstallPathBtn New Value : ' + window.$("#configuration_edit_installpath").html() )
            console.log('configurationEditInstallPathBtn Old Value: ' + currentConfiguration.segment._install_path)

            if( window.$("#configuration_edit_installpath").html() == configurationEditInstallPathBtnPreviousValue)
                return

            currentConfiguration.segment.group._install_path = window.$("#configuration_edit_installpath").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
        })

        configurationEditNameBtn.addEventListener('blur', (event, args) => {
            console.log('configurationEditNameBtn New Value : ' + window.$("#configuration_edit_name").html() )
            console.log('configurationEditNameBtn Old Value: ' + currentConfiguration.segment._name)


            if( window.$("#configuration_edit_name").html() == configurationEditNameBtnPreviousValue)
                return

            currentConfiguration.segment._name = window.$("#configuration_edit_name").html()
            configurationPreviousEditUuid = currentConfiguration.segment.uuid
            configurationEditNameBtnPreviousValue = currentConfiguration.segment._name
            configurationEditCount++
            currentConfiguration.segment.uuid = uuidv1()
            ipcRenderer.send('configuration-edited-name', currentConfiguration.segment._name)
        })

        //2. Trigger IPC Chromium events
        //Listen for events
        ipcRenderer.on('configuration-state-update' , (event,  args) => {
            console.log(' ipc renderer : configuration-state-update : ' + args)

            //todo a.p make something kewl.
            //var tempState = 'Ready'

            //if(currentConfiguration.state != null && currentConfiguration.value == '')
            //    tempState = currentConfiguration.state

            //configurationState
            //configurationState.innerHTML = tempState

            switch(currentConfiguration.state){
            case (standby)       : configurationState.innerHTML ='Standby' ; break;
            case (conf_selected) : configurationState.innerHTML ='Configuring' ; break;
            case (synching)      : configurationState.innerHTML ='Synching' ; break;
            case (synched)       : configurationState.innerHTML ='Synched' ; break;
            case (loading)       : configurationState.innerHTML ='Loading' ; break;
            case (running)       : configurationState.innerHTML ='Running' ; break;
            case (unloading)     : configurationState.innerHTML ='Unloading' ; break;
            }
        })

        ipcRenderer.on('configuration-edited-name' , (event,  args) => {
            console.log(' ipc renderer : configuration-edited-name : ' + args)
            if ( currentConfiguration != '' ) {

                currentConfiguration.segment._name
                configurations.forEach( function(configurationEntry) {

                    if ( configurationEntry.segment._name == args ) {
                        currentConfigurationName = args
                        currentConfiguration = configurationEntry
                        document.getElementById('configuration_select').innerHTML += '<option value="'+configurationEntry.segment._name+' selected="true">'+configurationEntry.segment._name+'</option>'
                        ipcRenderer.send('reset-fancytree', currentConfiguration)
                        ipcRenderer.send('select-configuration-updateselecthtml', currentConfiguration.segment._name)
                    }
                })
            }
        })
        ipcRenderer.on('render-configuration-table' , (event,  args) => {
            console.log(' ipc renderer : render-configuration-table : ' + args)
            if ( currentConfiguration != '' ) {
                console.log(' calling update-node-status !@! ')
                renderJQueryTree(args)
                this.isChanged = false
            }
        })
        ipcRenderer.on('update-node-status' , (event,  args) => {
            console.log(' ipc renderer : update-node-status : ' + args)
            if ( currentConfiguration != '' ) {
                console.log(' calling update-node-status !@! ')
                updateJQueryTreeNodeStatus(args)
            }
        })
        ipcRenderer.on('reset-configuration' , (event,  args) => {
            console.log(' ipc renderer : reset-configuration : ' + currentConfiguration)
            if ( currentConfiguration != '' ) {
                console.log(' calling resetConfiguration !@! ')
                resetConfiguration()
            }
        })
        ipcRenderer.on('save-configuration' , (event,  args) => {
            if ( currentConfiguration != '' ) {
                console.log(' ipc renderer : save-configuration : ' + currentConfiguration.segment._name)
                saveConfiguration(currentConfiguration)
            }
        })
        ipcRenderer.on('print-configuration' , (event,  args) => {
            console.log(' ipc renderer : print-configuration')
            console.log(currentConfiguration)
        })
        ipcRenderer.on('create-configuration' , (event,  args) => {
            console.log(' ipc renderer : create-configuration')
            var newConfiguration = createNewConfiguration()
            currentConfiguration = newConfiguration //debatable
            ipcRenderer.send('select-configuration', newConfiguration.segment._name)

        })
        ipcRenderer.on('create-runner' , (event,  args) => {
            console.log(' ipc renderer : create-configuration')
            createRunner()
            ipcRenderer.send('select-configuration', currentConfiguration.segment._name)
        })
        ipcRenderer.on('create-packagegroup' , (event,  args) => {
            console.log(' ipc renderer : create-packagegroup')
            createPackageGroup()
            ipcRenderer.send('reset-fancytree')
        })
        ipcRenderer.on('create-package' , (event,  args) => {
            console.log(' ipc renderer : create-package')
            createPackage()
            ipcRenderer.send('reset-fancytree')
        })
        //delete section
        ipcRenderer.on('delete-node' , (event,  args) => {
            console.log(' ipc renderer : delete-node')
            deleteNode()
            ipcRenderer.send('select-configuration', currentConfiguration.segment._name)
        })
        ipcRenderer.on('delete-packagegroup' , (event,  args) => {
            console.log(' ipc renderer : delete-packagegroup')
            deletePackageGroup()
            ipcRenderer.send('select-configuration', currentConfiguration.segment._name)
        })
        ipcRenderer.on('delete-package' , (event,  args) => {
            console.log(' ipc renderer : delete-package')
            deletePackage()
            ipcRenderer.send('select-configuration', currentConfiguration.segment._name)
        })
        ipcRenderer.on('select-node' , (event,  args) => {
            console.log(' ipc renderer : select-node : ' + args)

            if(currentConfiguration == ''){
                console.log('select-node --- NO CURRENT CONFIGURATION POINTER !!! ')
                return
            }

            currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
                if(nodeEntry.uuid == args){
                    currentNode = nodeEntry
                    currentSelectedObj = nodeEntry
                    console.log('select-node FOUND : ' + currentSelectedObj.uuid)
                    return ;
                }
            })
        })
        ipcRenderer.on('select-packagegroup' , (event,  args) => {
            console.log(' ipc renderer : select-packagegroup : ' + args)

            if(currentConfiguration != '') {

                var returnUuidObj = ''
                currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
                    nodeEntry.packageGroup.forEach(function(packageGroupEntry) {
                        recursivePkgGroupFind(packageGroupEntry, args)
                    })
                })
            } else {
                console.log('Warning - select-package : No Current CONF Pointer !!!')
            }
        })

        //select-packagegroup -- helper method.
        function recursivePkgGroupFind(ptrPkgGroup, args){
            if(ptrPkgGroup.uuid == args) {
                currentPackageGroup = ptrPkgGroup
                currentSelectedObj = ptrPkgGroup
                return ptrPkgGroup;
            }
            ptrPkgGroup.packageGroup.forEach( function(pkgGroupEntry) {
                recursivePkgGroupFind(pkgGroupEntry, args)
            })
        }

        function visitPackageGroupEntryForSelection(aPackageGroupEntry){

            if(aPackageGroupEntry.packageGroup != undefined && aPackageGroupEntry.packageGroup.length > 0){
                aPackageGroupEntry.packageGroup.forEach( function(packagegrpEntry) {

                    visitPackageGroupEntryForSelection(packagegrpEntry)

                    packagegrpEntry.package.forEach( function(packageEntry) {
                        if(packageEntry.uuid == args) {
                            currentPackage = packageEntry
                            currentSelectedObj = packageEntry
                            return ;
                        }
                    })
                })
            }
            else return
        }

        ipcRenderer.on('select-package' , (event,  args) => {
            console.log(' ipc renderer : select-package : ' + args)

            if(currentConfiguration != '') {

                currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
                    nodeEntry.packageGroup.forEach(function(packageGroupEntry) {

                        visitPackageGroupEntryForSelection(packageGroupEntry)
                        packageGroupEntry.package.forEach( function(packageEntry) {
                            if(packageEntry.uuid == args) {
                                currentPackage = packageEntry
                                currentSelectedObj = packageEntry
                                return ;
                            }
                        })
                    })
                })
            } else {
                console.log('Warning - select-package : No Current CONF Pointer !!!')
            }
        })
        ipcRenderer.on('select-configuration' , (event,  args) => {
            console.log('Renderer : select-configuration : ' + args)

            if (args != '') {
                configurations.forEach( function(configurationEntry) {

                    if ( configurationEntry.segment._name == args ) {
                        currentConfigurationName = args
                        currentConfiguration = configurationEntry
                        ipcRenderer.send('select-configuration-updateselecthtml', configurationEntry.segment._name)
                        ipcRenderer.send('configuration-update-desc')
                        ipcRenderer.send('reset-fancytree', currentConfiguration)
                        ipcRenderer.send('configuration-generate-archives', configurationEntry)
                    }
                    else {

                    }
                })
            }
        })

        ipcRenderer.on('configuration-update-desc' , (event, args) => {
                updateConfigurationPropertyTreeUi()
        })

        ipcRenderer.on('select-configuration-updateselecthtml' , (event, args) => {
            console.log('select-configuration-updateselecthtml ! : ' + args)
            document.getElementById('configuration_select').innerHTML = ''
            document.getElementById('configuration_select').innerHTML += '<option value="none">Please choose</option>'

            /**/
            configurations.forEach( function(configurationEntry) {
                //if ( currentConfiguration.segment._name == configurationEntry.segment._name ) {
                //    document.getElementById('configuration_select').innerHTML += '<option value="'+configurationEntry.segment._name+' selected="true">'+configurationEntry.segment._name+'</option>'
                //}
                //else {
                    document.getElementById('configuration_select').innerHTML += '<option value="'+configurationEntry.segment._name+'">'+configurationEntry.segment._name+'</option>'
                //}
            })

            window.$("#configuration_select option[value=" +args + "]").prop('selected', true)
        })
        ipcRenderer.on('configuration-tempfolder-create' , (event,  args) => {
            var folderExist = fs.existsSync(CASTORPOUTINE_ROOT_FOLDER + '/' + args + '/temp' )

            if(!folderExist){
                fs.mkdir( CASTORPOUTINE_ROOT_FOLDER + '/' + args + '/temp',  (err) => {
                    if (err) throw err
                    console.log('Temp folder created : ' + args)
                });
            }
        })

        ipcRenderer.on('reset-fancytree' , (event) => {
            resetJQueryFancyTree()
            updateJQueryFancyTree(currentConfiguration)
            //todo a.p this refresh the UI when changing conf name...
            updateConfigurationPropertyTreeUi(currentConfiguration)
        })
        /*  stacked in reset cause async cause problem.
        ipcRenderer.on('update-fancytree', (event, arg) => {
            updateJQueryFancyTree(arg)
        })
        ipcRenderer.on('update-configuration-propertytreeui', (event, arg) => {
            updateConfigurationPropertyTreeUi(arg)
        })
*/
        ipcRenderer.on('open-save-configuration-dialog', (event, arg) => {
            console.log('open-save-configuration-dialog : ' + arg )
            if(arg == 0){
                console.log('open-save configurations LENGTH  : ' + configurations.length )
                console.log('getCurrentConfigurationName  : ' + getCurrentConfigurationName() )
                configurations.forEach( function(configurationEntry) {

                    if ( configurationEntry.segment._name == getCurrentConfigurationName() ) {
                        console.log('Saving config named : ' + getCurrentConfigurationName())
                        fs.writeFile('/castorpoutine/conf/' + getCurrentConfigurationName() + '/' + getCurrentConfigurationName() + '.json', JSON.stringify(configurationEntry),(err) => {
                          if (err) throw err;
                          console.log('The file has been saved!')
                        })
                        return
                    }
                })
            }
        })

        //REST
        ipcRenderer.on('REST_NODE_STATUS' , (event,  args) => {
            //TODO A.P control logs. console.log(' REST_NODE_STATUS : ' + args)
            console.log('Renderer - ConfiguratioCtrl : REST_NODE_STATUS : ')
            console.log(args)
            if(currentConfiguration == '')
                return

            var bFound = false
            currentConfiguration.segment.group.node.forEach(function(nodeEntry) {
                //TODO A.P control logs. console.log(' REST_NODE_STATUS : ' + args) console.log('validating node reset status ' + nodeEntry._name)
                if(nodeEntry._name == args.name){
                    nodeEntry.httpStatus = args.status + ' - '+ args.state
                    bFound = true
                }
            })

            // Cause whole page to glitch.
            //if(bFound)
            //    ipcRenderer.send('reset-fancytree')
        })
        //SSH
        ipcRenderer.on('SSH_NODE_STATUS' , (event,  args) => {

            if(currentConfiguration == '')
                return

            var bFound = false
            currentConfiguration.segment.group.node.forEach(function(nodeEntry) {
                //TODO A.P control logs. console.log(' REST_NODE_STATUS : ' + args) console.log('validating node reset status ' + nodeEntry._name)
                if(nodeEntry._name == args.name && nodeEntry.nodeStatus == (args.status + ' - '+ args.state) ){
                    nodeEntry.nodeStatus = 'SSH : OK'
                    bFound = true
                }
            })

            //if(bFound)
            //    ipcRenderer.send('reset-fancytree')
        })
    }


    saveTempConfiguration(){
        console.log('Renderer Inteval Save : ' + currentConfigurationName)
        var stringifyedConf = JSON.stringify(currentConfiguration)

        //check if temp folder exist.
        var folderExist = fs.existsSync(CASTORPOUTINE_CONF_FOLDER_PATH + '/' + currentConfigurationName + '/temp' )

        if(!folderExist){
            fs.mkdir( CASTORPOUTINE_CONF_FOLDER_PATH + '/' + currentConfigurationName + '/temp',  (err) => {
                if (err) throw err
                console.log('Saved configuration : ' + currentConfigurationName)
            });
        }
        fs.writeFile( CASTORPOUTINE_CONF_FOLDER_PATH + '/' + currentConfigurationName + '/temp/' + currentConfigurationName + '.' + String(configurationEditCount) + '.json', stringifyedConf, (err) => {
          if (err) throw err
          console.log('The config currentConfigurationName has been saved!')
          configurationEditCount++
          configurationPreviousEditUuid = currentConfiguration.segment.uuid
        });
    }
    constructor () {
        console.log('Initializing CLASS : ConfigurationCtrl')

        //1. 1st operation, read all configuration in the conf folder and make them available to renderers.
        //1.1 Create castorpoutine folder if doesnt exist
        createFileSystemDirectory()

        var result = fs.readdirSync( CASTORPOUTINE_CONF_FOLDER_PATH )

        if(typeof result != "undefined" && result.length > 0) {
            for (var i = 0 ; i < result.length; i++) {

                var fullPathToJson = CASTORPOUTINE_CONF_FOLDER_PATH + '/' + result[i] + '/' + result[i] + '.json'
                var folderExist = fs.existsSync(fullPathToJson )

                if(folderExist){
                    var fileBuffer = fs.readFileSync(fullPathToJson) //todo a.p make more robust.
                    configurations.push(JSON.parse(fileBuffer))
                }
            }

            configurations.forEach( function(entry) {
                document.getElementById('configuration_select').innerHTML += '<option value="'+entry.segment._name+'">'+entry.segment._name+'</option>'

                //reset UUID of elements
                entry.segment.group.node.forEach(function(nodeEntry) {
                    nodeEntry.uuid = uuidv1()
                    nodeEntry.sshStatus = 'SSH : OFFLINE'
                    nodeEntry.httpStatus = 'HTTP : OFFLINE'
                    nodeEntry.isChanged = true
                })
            })
        }

        console.log('before : this.initEventListeners()')
        this.initEventListeners()
        console.log('after : this.initEventListeners()')
    }
}

//********************************************************************************************
//global utility methods
function renderJQueryTree(){
    tree.reload()
    //currentConfiguration.segment.group.node[0]._name = "W_REFRESH_W"
    //currentConfiguration.segment.group.node[0].reload()
    /*
    currentConfiguration.segment.group.node.forEach( function(entry) {

        //reset UUID of elements
        entry.node.forEach(function(nodeEntry) {
            if(nodeEntry.isChanged == true){
                nodeEntry.reload()
            }
            //[packagegroup]
            nodeEntry.children.forEach(function(pkgGroup) {
                if(pkgGroup.isChanged == true){
                    pkgGroup.reload()
                }
                pkgGroup.children.forEach(function(packageEntry) {
                    if(packageEntry.isChanged == true){
                        packageEntry.reload()
                    }
                })
            })
        })
    })

    currentConfiguration.isChanged = false
    */
}

function renderJQueryTreevisitPackageGroup(aPackageGroup){
    if(packageGroupEntry.packageGroup != '' && packageGroupEntry.packageGroup != undefined && packageGroupEntry.packageGroup.length > 0 )
        renderJQueryTreevisitPackageGroup.reload()
}

function updateJQueryTreeNodeStatus(args){
    //tree.reload()
    var tree = $.ui.fancytree.getTree("#tree");

    var tobeExpended = new Array();

    tree.visit(function(node){
      if ( node.isExpanded() ){
        tobeExpended.push(node.uuid)
      }
    });

    $('#tree').fancytree('option', 'source', args.segment.group.node);

    // Expand all tree nodes
    tree.visit(function(node){
        if ( tobeExpended.includes(node.uuid) ){
             node.setExpanded(true);
        }
    });
}

function resetConfiguration(){
    currentNode = ''
    currentPackage = ''
    currentConfiguration  = ''
    currentPackageGroup = ''
    currentSelectedObj  = ''
    currentSelectedNode  = ''

    resetConfigurationPropertyTreeUi()

    window.$("#tree_selection_container_packagegroup").hide()
    window.$("#tree_selection_container_package").hide()
    window.$("#tree_selection_container_node").hide()

    resetJQueryFancyTree()
}
function saveConfiguration(configurationReference){
    console.log('Saving configuration : ' + configurationReference.segment._name)
    var stringifyedConf = JSON.stringify(configurationReference)

    //check if temp folder exist.
    var folderPath = CASTORPOUTINE_CONF_FOLDER_PATH + '/' + configurationReference.segment._name
    var fullPathToJson = CASTORPOUTINE_CONF_FOLDER_PATH + '/' + configurationReference.segment._name + '/' + configurationReference.segment._name + '.json'
    var folderExist = fs.existsSync( folderPath )

    // Check if the file exists in the current directory, and if it is writable.
    fs.access(folderPath, fs.constants.F_OK | fs.constants.W_OK, (err) => {
      if (err) {
        fs.mkdir( folderPath ,  (err) => {
            if (err) throw err
            console.log('mkdir configuration : ' + folderPath)
        })
      }
    })

    fs.open(fullPathToJson, 'wx', (err, fd) => {
        if (err) {
            if (err.code === 'EEXIST') {
                console.error('myfile already exists');
                fs.renameSync(fullPathToJson, fullPathToJson+".prev")
            }
        }

        fs.writeFile( fullPathToJson, beautify(stringifyedConf, { indent_size: 4, space_in_empty_paren: true }), (err) => {
            if (err) throw err
                console.log('The config currentConfigurationName has been saved!')
        })
    })
}

function visitPackageGroup(entry, countVisit) {

    console.log('visitPackageGroup countVisit : ' + countVisit)

    if (typeof entry == "undefined") {
        console.log('entry undefined')
        return ;
    }

    entry['folder'] = true
    entry['data'] = {
                    "icon": "icons.gif",
                    "isLocked": true
                    }
    entry['icon_url'] = 'url(assets/3rdparty/linux/linux.png)'

    if(countVisit > 0){
        entry['fancytreeNodeType'] = 'packagegroup'
        entry['title'] = entry._name

        entry['key'] = entry.uuid
        entry['children'] = entry.packageGroup
        entry['children']['key'] = entry.uuid
        entry['folder'] = true
        entry['title'] = entry._name
        entry['expanded'] = entry.expanded
        entry['_syncStatus'] = false
        entry['key'] = entry.uuid
        entry['nodeStatus'] = '!'
        entry['sshStatus'] = '!'
        entry['httpStatus'] = '!'
    }
    else{
        entry['fancytreeNodeType'] = 'node'
        entry['title'] = entry._name

        entry['key'] = entry.uuid
        entry['children'] = entry.packageGroup
        //entry['children']['key'] = entry.uuid
        entry['folder'] = true
        entry['title'] = entry._name
        entry['expanded'] = entry.expanded
        entry['_syncStatus'] = false
        entry['key'] = entry.uuid
        //entry['children']['key'] = entry.uuid

        if(entry['nodeStatus'] == '')
            entry['nodeStatus'] = 'xxxOFFLINE!'

        entry['expanded'] = entry.expanded
    }

    if( typeof entry.package != 'undefined') {
        if(Array.isArray(entry.package) && entry.package.length > 0){

            entry.package.forEach(function(packageEntry) {
                visitPackage(packageEntry)
                entry['children'] = entry.package
                entry['children']['key'] = entry.uuid
            })
        } else {
            console.log('should never happen !!!')
        }
    }

    if( typeof entry.packageGroup != 'undefined') {

        entry.packageGroup.forEach(function(entryGroup) {
            if(Array.isArray(entryGroup.packageGroup)){
                visitPackageGroup(entryGroup, 1)
            }
        })
    }
    return entry
}

function visitPackage(entryPackage){
    console.log('visit package')
    if (typeof entryPackage == "undefined") {
        console.log('entry undefined')
        return ;
    }

    //entryPackage['children'] = null
    entryPackage['title']                = entryPackage._name
    entryPackage['fancytreeNodeType']    = 'package'
    entryPackage['folder']               = false
    entryPackage['expanded']             = entryPackage.expanded
    entryPackage['key']                  = entryPackage.uuid
    entryPackage['uuid']                 = entryPackage.uuid
    entryPackage['customClass']          = 'fa-file-archive'
    entryPackage['packageStatus']        = 'offline'
    entryPackage['icon_url'] = 'url(assets/3rdparty/msft/apple.png)'
}

function resetJQueryFancyTree() {

    var tableString = '<table id="tree">' +
        '<colgroup>' +
        '<col width="40%" align="center"></col>' +
        '<col width="15%" align="left"></col>' +
        '<col width="15%" align="left"></col>' +
        '<col width="15%"></col>' +
        '<col width="15%"></col>' +
        '</colgroup>' +
        '<thead>' +
            '<tr> <th>Node Name</th> <th>SSH Status</th><th>HTTP Status</th> <th>Packages Sync</th> <th>Classification</th></tr>' +
        '</thead>' +
        '<tbody>' +
                  '<tr> <td  align="left" /><td  align="left"></td><td  align="left"></td><td  /><td  align="left"/></tr>' +
        '</tbody>' +
    '</table>'

    window.$("#tree_container").html('');
    window.$("#tree_container").html(tableString)
}

function updateConfigurationPropertyTreeUi(){
    window.$("#configuration_version_integer").html(currentConfiguration.segment._version)
    window.$("#configuration_lastedit_date").html(currentConfiguration.segment._lastedit)
    window.$("#configuration_edit_name").html(currentConfiguration.segment._name)
    window.$("#configuration_edit_workingfolder").html(CASTORPOUTINE_CONF_FOLDER_PATH)
    window.$("#configuration_edit_groupsettings").html(currentConfiguration.segment.group._groupsettings)
    window.$("#configuration_edit_loadsequence").html(currentConfiguration.segment.group._loadsequence)
    window.$("#configuration_edit_installsequence").html(currentConfiguration.segment.group._installsequence)
    window.$("#configuration_edit_installpath").html(currentConfiguration.segment.group._install_path)
}
function updateConfigurationPropertyTreeUi(args){
    window.$("#configuration_version_integer").html(args.segment._version)
    window.$("#configuration_lastedit_date").html(args.segment._lastedit)
    window.$("#configuration_edit_name").html(args.segment._name)
    window.$("#configuration_edit_workingfolder").html(CASTORPOUTINE_CONF_FOLDER_PATH)//todo fix !  + "/" + args.segment._name + ".xml"
    window.$("#configuration_edit_groupsettings").html(args.segment.group._groupsettings)
    window.$("#configuration_edit_loadsequence").html(args.segment.group._loadsequence)
    window.$("#configuration_edit_installsequence").html(args.segment.group._installsequence)
    window.$("#configuration_edit_installpath").html(args.segment.group._install_path)
}
function resetConfigurationPropertyTreeUi(){
    window.$("#configuration_version_integer").html('')
    window.$("#configuration_lastedit_date").html('')
    window.$("#configuration_edit_name").html('')
    window.$("#configuration_edit_workingfolder").html('')
    window.$("#configuration_edit_groupsettings").html('')
    window.$("#configuration_edit_loadsequence").html('')
    window.$("#configuration_edit_installsequence").html('')
    window.$("#configuration_edit_installpath").html('')
}

function updateJQueryFancyTree(configuration){

    configuration.segment.group.node.forEach(function(nodeEntry) {
        nodeEntry['expanded'] = nodeEntry.expanded
        //nodeEntry['customClass'] = 'fa fa-archive'
        nodeEntry['sshStatus'] = nodeEntry.sshStatus
        nodeEntry['httpStatus'] = nodeEntry.httpStatus
        nodeEntry['title'] = nodeEntry._name
        nodeEntry['fancytreeNodeType'] = 'node'
        nodeEntry['key'] = nodeEntry.uuid
        nodeEntry['icon_url'] = 'url(assets/3rdparty/msft/windows.png)'
        console.log('Node UUID : ' + nodeEntry.uuid)
        nodeEntry['folder'] = true
        nodeEntry['data'] = {
                            "icon": "icons.gif",
                            "isLocked": true
                            }
        //nodeEntry['icon'] = true
        //nodeEntry = visitPackageGroup(nodeEntry, countVisit)
        visitPackageGroup(nodeEntry, 0)
    })

    tree = fancytree.createTree('#tree', {
        checkbox: false,
        imagePath: "assets/app-icon",
        // icon: false,
        titlesTabbable: true,        // Add all node titles to TAB chain
        source: configuration.segment.group.node,//using group will allow us to display a configuration node ... ez ui
        extensions: ["table", "gridnav", "edit"],
        table: {
            checkboxColumnIdx: 0,    // render the checkboxes into the this column index (default: nodeColumnIdx)
            indentation: 75,         // indent every node level by 16px
            nodeColumnIdx: 0         // render node expander, icon, and title to this column (default: #0)
        },
        tooltip: function(event, data){
        return data.node.data._name;
        },
        gridnav: {
            autofocusInput:   false, // Focus first embedded input if node gets activated
            handleCursorKeys: true   // Allow UP/DOWN in inputs to move to prev/next node
        },
    	renderStatusColumns: false,	 // false: default renderer
                                     // true: generate renderColumns events, even for status nodes
                                     // function: specific callback for status nodes
        click: function(event, data) {

            //prevent double click,
/*
            if (clickcount == 1) {
                clickcount = 0
                return
            }
            clickcount++;
            */
            //var exp1,exp2 =data.node.expanded//
            window.$ = window.jQuery = require('jquery')
            data.node.expanded = !data.node.expanded
            data.node.data.expanded = !data.node.data.expanded
            //exp2 =data.node.expanded
            data.node.data.isChanged = true
            data.node.data.tooltip = "FILL_ME"
            data.node.data.selected = true

            if(currentSelectedObj != '' && currentSelectedObj != undefined && data.node.data.uuid == currentSelectedObj.uuid){
                //currentSelectedObj.expanded = !currentSelectedObj.expanded
                //currentSelectedObj.selected = false
                console.log('double click')
            }

            switch (data.node.data.fancytreeNodeType) {
                case 'node' : {

                        console.log('selected node ')
                        console.log(data.node.data)

                        ipcRenderer.send('select-node', data.node.data.uuid)

                        window.$("#node_edit_name").html(data.node.data._name)
                        window.$("#node_edit_username").html(data.node.data.username)
                        window.$("#node_edit_ip").html(data.node.data.ip)
                        window.$("#node_edit_os").html(data.node.data.os)
                        window.$("#node_edit_sshkey").html(data.node.data.sshkey)
                        window.$("#node_edit_sshpwd").html(data.node.data.sshpwd)
                        window.$("#node_edit_port").html(data.node.data.port)
                        window.$("#node_edit_settings").html(data.node.data.settings)
                        window.$("#node_edit_env").html(data.node.data.env)
                        window.$("#node_edit_performance").html(data.node.data.performance)

                        window.$("#tree_selection_container_packagegroup").hide()
                        window.$("#tree_selection_container_package").hide()
                        window.$("#tree_selection_container_node").show()
                } break;
                case 'packagegroup' : {
                    console.log('packagegroup click')
                    console.log(data.node.data)
                    ipcRenderer.send('select-packagegroup', data.node.data.uuid)

                    window.$("#packagegroup_edit_name").html(data.node.data._name)

                    window.$("#tree_selection_container_packagegroup").show()
                    window.$("#tree_selection_container_package").hide()
                    window.$("#tree_selection_container_node").hide()
                } break;
                case 'package' : {
                    console.log('package click')
                    console.log(data.node.data)
                    ipcRenderer.send('select-package', data.node.data.uuid)
                    window.$("#package_edit_name").html(data.node.data._name )
                    window.$("#package_edit_alias").html(data.node.data._alias )
                    window.$("#package_edit_installpath").html(data.node.data._install_path )
                    window.$("#package_edit_location").html(data.node.data._location )
                    window.$("#package_edit_xmlLayerFile").html(data.node.data._xmlLayerFile )
                    window.$("#package_edit_installsequence").html( data.node.data._installsequence )
                    window.$("#package_edit_loadsequence").html( data.node.data._loadsequence )
                    window.$("#package_edit_settings").html(data.node.data._settings)
                    window.$("#package_edit_cminfo").html(data.node.data._cmInfo)

                    window.$("#tree_selection_container_packagegroup").hide()
                    window.$("#tree_selection_container_package").show()
                    window.$("#tree_selection_container_node").hide()
                } break;
                default : {console.log('Selected data has no fancytree node type of node, packagegroup, package type')} break;
            }
        },
        /*
        tooltip: function(event, data){
            return data.node.data.author;
        },
        lazyLoad: function(event, data) {
            data.result = {url: "ajax-sub2.json"}
        },
        */
        // Optionally tweak data.node.span

      renderNode: function(event, data) {
            var node = data.node;
            if(node.data.icon_url){
              var $span = $(node.span);

              //$span.find("> span.fancytree-title").text(">> " + node.title).css({
                //fontStyle: "italic"
              //});

            var iconUrl = ''

            if(data.node.data.os == "Linux")
                iconUrl = 'url(assets/3rdparty/linux/linux.png)'
            else if(data.node.data.os == "Windows")
                iconUrl = 'url(assets/3rdparty/windows/windows.png)'
            else if(data.node.data.os == "Apple")
                iconUrl = 'url(assets/3rdparty/apple/apple.png)'
            //else if(data.node.data.os == "BSD")
            //    iconUrl = 'url(assets/3rdparty/apple/bsd.png)'

              switch (data.node.data.fancytreeNodeType) {
                  case 'node' : {
                      $span.find("> span.fancytree-icon").css({
                          //border: "1px solid green",
                          //backgroundImage: node.icon_url,
                          backgroundImage: iconUrl,//"url(assets/3rdparty/apple/apple_red.png)",
                          backgroundPosition: "0 0",
                          backgroundSize: "cover"
                          });
                      }
                      default : {console.log('Rendered data node has no fancytree node type of node, packagegroup, package type')} break;
                  }


            }
        },
        renderColumns: function(event, data) {
            var node = data.node;
            var tdList = $(node.tr).find(">td");

            tdList.eq(0).find(".fancytree-title").text(node.data._name)
            //console.log(value[0])
                        //value[0].textContent(node.data._name)
            switch (data.node.data.fancytreeNodeType) {
                case 'node' : {
                    //tdList.eq(0)

                    tdList.eq(1)
                        .text(node.data.sshStatus)
                        //.addClass(node.customClass)
                        //.addClass("alignRight")
                    tdList.eq(2)
                        .text(node.data.httpStatus)
                        //.addClass(node.customClass)
                        //.addClass("alignRight")
                    tdList.eq(3)
                        .text(node.data._install_path)
                        //.addClass(node.customClass)
                        //.addClass("alignRight")
                        .contextmenu(function(e){
                          ipcRenderer.send('show-packagegroup-context-menu', null)
                        })
                    tdList.eq(4)
                        .text(node.data._xmlLayerFile)
                        //.addClass(node.customClass)
                        //.addClass("alignRight")
                        .contextmenu(function(e){
                          ipcRenderer.send('show-package-context-menu', null)
                        })
                    tdList.eq(5)
                        .text(node.data.title)
                        //.addClass(node.customClass)
                        //.addClass("alignRight")
                }
                default : {console.log('Rendered data node has no fancytree node type of node, packagegroup, package type')} break;
            }
        }
    })

    //tree.expandAll(true)
}

function getCurrentConfigurationName() {
    //todo make more robust
    if(currentConfiguration != '')
        return currentConfiguration.segment._name
    return ''
}

//CRUD factory
function createPackage() {

    newPackage = {}
    newPackage.uuid = uuidv1()
    newPackage._name = 'DEFAULT_PKG_' + newPackageCount
    newPackageCount++
    //newPackage.title = newPackage._name
    newPackage._alias = 'greentool'
    newPackage._autoVersionOnDisk = true
    newPackage._cmInfo = 'git://my_machine:49201/greentool'
    newPackage._cmURI = 'cm:greentool@GLOBAL#0.8.83.0'
    newPackage._contextURI = ''
    newPackage._install_path = 'Resources/Install'
    newPackage._installsequence = ''
    newPackage._loadsequence = ''
    newPackage._settings = ''
    newPackage._xmlLayerFile = newPackage._name + '_config.xml'

    console.log('craete package sel obj uuid : ' + currentSelectedObj.uuid)

    if(currentSelectedObj.uuid == currentPackageGroup.uuid) {
        currentPackageGroup.package.push(newPackage)
        currentSelectedObj = newPackage
        currentPackageGroup = newPackage
    }
    else {
        console.log('cant add package no group selected')
    }
}

function createPackageGroup(){

    newPackageGroup = {}
    newPackageGroup._name = 'PKG_GROUP_' + newPackageGroupCount
    newPackageGroupCount++
    newPackageGroup.uuid = uuidv1()
    newPackageGroup.package = new Array()
    newPackageGroup.packageGroup = new Array()

    if ( currentSelectedObj.uuid == currentPackageGroup.uuid) {
        console.log('adding to pkg group')
        currentPackageGroup.packageGroup.push(newPackageGroup)
    } else if(currentSelectedObj.uuid == currentNode.uuid) {
        console.log('adding to node uuid')
        console.log(currentNode.uuid)
        currentNode.packageGroup.push(newPackageGroup)
    }

    currentSelectedObj = newPackageGroup
    currentPackageGroup = newPackageGroup
}

function createRunner(){

    var newNode = {}
    newNode.uuid = uuidv1()
    newNode._name = 'RUNNER_NEW ' + newNodeCount
    newNodeCount++
    newNode._title = newNode._name
    newNode.os = 'Linux'
    newNode.ip = '192.168.1.1'
    newNode._cmInfo = 'disk'
    newNode._cmURI = 'RUNNER_NEW@myconfig'
    newNode._contextURI = 'RUNNER_NEW'
    newNode._install_path = ''
    newNode._installsequence = 'installsequence.xml'
    newNode._loadsequence = 'loadsequence.xml'
    newNode._settings = 'settings.xml'
    //newNode._workingFolder = ''
    newNode.packageGroup = new Array()
    newNode.children = newNode.packageGroup

    currentConfiguration.segment.group.node.push(newNode)

    currentSelectedObj = newNode
    currentNode = newNode
}

function deleteRunner(){

    currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
        if(nodeEntry.uuid == currentSelectedNode.uuid){
            currentConfiguration.segment.group.node.remove(nodeEntry)
        }
    })

    currentSelectedObj = ''
    currentNode = ''
}

function createNewConfiguration() {
    console.log('function createNewConfiguration')

    var newConfiguration = {}
    newConfiguration.uuid = uuidv1()

    var newSegment = {}
    newSegment.uuid = uuidv1()
    newSegment._description = 'New description'
    newSegment._masternode = newNodeRunnerPrefix
    newSegment._name = newConfigurationPrefix + moment().format('YYYY_MM_DD_h_mm_ss')
    newConfigurationCount++
    newSegment.lastedit = moment().format('YYYY:MM:DD h:mm:ss')

    newConfiguration.segment = newSegment

    var simGroup = {}
    simGroup._buildSettings ='buildsettings.xml'
    simGroup._cmInfo='disk'
    simGroup._cmURI='cm:SIM@M_CONF_1'
    simGroup._contextURI='xmllayer.ant:simconfig.xml'
    simGroup._installsequence='installsequence.xml'
    simGroup._loadsequence = 'loadsequence.xml'
    simGroup._name='CASTORPOUTINE'
    simGroup._settings='groupsettings_lab.xml'
    //simGroup._workingFolder='d:\\castorpoutine\\config\\M_CONF_1'
    simGroup._workingFolder = CASTORPOUTINE_CONF_FOLDER_PATH + '/M_CONF_1'

    simGroup._syncStatus = false
    simGroup.uuid = uuidv1()
    simGroup.node = new Array()

    newConfiguration.segment.group = simGroup

    var newNode = {}
    newNode.uuid = uuidv1()
    newNode._name = 'RUNNER_NEW_' + newNodeCount
    newNode.os = 'Linux'
    newNode.ip = '192.168.1.1'
    newNodeCount++
    newNode._title = newNode._name
    newNode._cmInfo = 'disk'
    newNode._cmURI = 'RUNNER_NEW@myconfig'
    newNode._contextURI = 'RUNNER_NEW'
    newNode._install_path = ''
    newNode._installsequence = 'installsequence.xml'
    newNode._loadsequence = 'loadsequence.xml'
    newNode._settings = 'settings.xml'
    newNode._workingFolder = CASTORPOUTINE_CONF_FOLDER_PATH + '/M_CONF_1'
    newNode.packageGroup = new Array()
    newNode.children = newNode.packageGroup
    newConfiguration.segment.group.node.push(newNode)

    configurations.push(newConfiguration)

    console.log('why are not adding')
    //add to html ?
    configurationSelectBtn.innerHTML += '<option value="'+  newSegment._name + '">'+  newSegment._name + '</option>'
    //window.$("#configuration_select option[value=" +newNode._name + "]").prop('selected', true)

    return newConfiguration
}

//### /castorpoutine/conf
function createFileSystemDirectory(){
    //todo simplify ...
    //echo 'createFileSystemDirectory()  CASTORPOUTINE_CONF_FOLDER_PATH=' + CASTORPOUTINE_CONF_FOLDER_PATH
    var folderExist = fs.existsSync( CASTORPOUTINE_CONF_FOLDER_PATH )

    if(!folderExist){
        fs.mkdir( CASTORPOUTINE_CONF_FOLDER_PATH,  (err) => {
            if (err) throw err
            console.log('Folder created : ' + CASTORPOUTINE_CONF_FOLDER_PATH)
        })
    }

    folderExist = fs.existsSync( CASTORPOUTINE_CONF_FOLDER_PATH )

    if(!folderExist){
        fs.mkdir( CASTORPOUTINE_CONF_FOLDER_PATH,  (err) => {
            if (err) throw err
            console.log('Folder created : ' + CASTORPOUTINE_CONF_FOLDER_PATH)
        });
    }

    folderExist = fs.existsSync( CASTORPOUTINE_TEMP_FOLDER_PATH )

    if(!folderExist){
        fs.mkdir( CASTORPOUTINE_TEMP_FOLDER_PATH,  (err) => {
            if (err) throw err
            console.log('Folder created : ' + CASTORPOUTINE_TEMP_FOLDER_PATH)
        })
    }
}

function findElementByUuid(uuid){
    //ipcRenderer.on('select-package' , (event,  args) => {

    currentConfiguration.segment.group.node.forEach( function(nodeEntry) {
        if(nodeEntry.uuid == uuid)
            return nodeEntry
        nodeEntry.packageGroup.forEach( function(packageGroupEntry) {
            if(packageGroupEntry.uuid == uuid)
                return packageGroupEntry
            packageGroupEntry.package.forEach( function(packageEntry) {
                if(packageEntry.uuid == uuid)
                    return packageEntry
            })
        })
    })
}
//Best class lenght ever. 2019-08-01 where we code one line we code all lines
