//INCLUDES
const {ipcRenderer} = require('electron')
//const pidusage = require('pidusage')
//const request = require('request')
const moment = require('moment')
const uuidv1 = require('uuid/v1')
const jqueryui = require('jquery-ui')
const fancytree = require('jquery.fancytree')

//******************************************************************************
//** SETUP FOLDERS LOGIC FOR GIVEN RUN
fs = require('fs')
fsextra = require("fs-extra");
os = require('os')

CASTORPOUTINE_ROOT_FOLDER       = process.env.CASTORPOUTINE_ROOT_FOLDER         //  value : /castorpoutine /var/castorpoutine d:/castorpoutine.
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/conf /var/castorpoutine/temp d:/castorpoutine/conf
CASTORPOUTINE_TEMP_FOLDER_PATH  = process.env.CASTORPOUTINE_TEMP_FOLDER_PATH    //  value : /castorpoutine/temp /var/castorpoutine/temp d:/castorpoutine/temp

var sshSessionsMap = new Map();
var httpSessionsMap = new Map();

classname = 'SSHCtrl'
module.exports = class SSHCtrl {

    constructor () {
        console.log('Initializing CLASS : ' + classname)
    }

    addSshSession(pGUID, pSshObj) {

        var pSshSession = sshSessionsMap.get(pGUID)

        if(typeof pSshSession === 'undefined') {
            sshSessionsMap.set(pGUID, pSshObj)
            console.log('Setting SSH session for GUID : ' + pGUID)
        }
    }

    getSshSession(pGUID) {

        var pSshSession = sshSessionsMap.get(pGUID)

        if(typeof pSshSession != 'undefined') {
            return pSshSession
        }

        return 'undefined'
    }
    addHTTPSession(pGUID, pHTTPObj) {

        var pSshSession = httpSessionsMap.get(pGUID)

        if(typeof pSshSession === 'undefined') {
            httpSessionsMap.set(pGUID, pHTTPObj)
            console.log('Setting HTTP session for GUID : ' + pGUID)
        }
    }

    getHTTPSession(pGUID) {

        var pHTTPSession = httpSessionsMap.get(pGUID)

        if(pHTTPSession != 'undefined') {
            return pHTTPSession
        }

        return 'undefined'
    }
}

function demoMethod(){

}
