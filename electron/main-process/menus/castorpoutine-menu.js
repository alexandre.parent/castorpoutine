const {
  BrowserWindow,
  Menu,
  MenuItem,
  ipcMain,
  app
} = require('electron')

// globalContextMenu
// ====================================================================
const globalContextMenu = new Menu()
globalContextMenu.append(new MenuItem({ label: 'Hello' }))
globalContextMenu.append(new MenuItem({ type: 'separator' }))
globalContextMenu.append(new MenuItem({ label: 'Electron', type: 'checkbox', checked: true }))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('global-context-menu', (e, params) => {
    globalContextMenu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-global-context-menu', (event) => {
  const win = BrowserWindow.fromWebContents(event.sender)
  globalContextMenu.popup(win)
})

// Simulation Context Menu
// ====================================================================
const simulationContextMenu = new Menu()
simulationContextMenu.append(new MenuItem({ label: 'Simulation Properties' }))
simulationContextMenu.append(new MenuItem({ type: 'separator' }))
//nodeContextMenu.append(new MenuItem({ label: 'Configuration Management', type: 'checkbox', checked: true }))
simulationContextMenu.append(new MenuItem({ label: 'Add Node', click: function() {

}}))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('simulation-context-menu', (e, params) => {
    simulationContextMenu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-simulation-context-menu', (event) => {
  console.log('show-simulation-context-menu !!!@!')
  const win = BrowserWindow.fromWebContents(event.sender)
  simulationContextMenu.popup(win)
})

// Node Context Menu
// ====================================================================
const nodeContextMenu = new Menu()
nodeContextMenu.append(new MenuItem({ label: 'Node Properties' }))
nodeContextMenu.append(new MenuItem({ type: 'separator' }))
//nodeContextMenu.append(new MenuItem({ label: 'Configuration Management', type: 'checkbox', checked: true }))
nodeContextMenu.append(new MenuItem({ label: 'Edit Node Properties', click: function() {
console.log('nodecontextmenu tes !! omg')
                                                                                        }
                                    }
                                    )
                        )

function addNewNode(data) {

}

function displayNodeEditModal(data) {
/*
    const path = require('path')
    const windowNodeEditModalPath = path.join('file://', __dirname, '../../sections/windows/modal'+data+'.html')
    let windowNodeEdit =  new BrowserWindow({frame: false})

    windowNodeEdit.on('close', () => {win = null})
    windowNodeEdit.loadURL(windowNodeEditModalPath)
    windowNodeEdit.show()

    // In the main process.
    const path = require('path')
    const { BrowserView, BrowserWindow } = require('electron')

    let win = new BrowserWindow({ width: 800, height: 600 })
    win.on('closed', () => {
      win = null
    })
    //const windowNodeEditModalPath = path.join('file://', __dirname, '../../modalViews/modal'+data+'.js')
    const windowNodeEditModalPath = path.join('file://', __dirname, '../../modalViews/modalNodeEdit.js')
    let view = new BrowserView()
    win.setBrowserView(view)
    view.setBounds({ x: 0, y: 0, width: 640, height: 480 })
    //view.webContents.loadURL('https://electronjs.org')
    view.webContents.loadURL(windowNodeEditModalPath)
    */


}
nodeContextMenu.append(new MenuItem({ label: 'Add Package Group'}))
nodeContextMenu.append(new MenuItem({ type: 'separator' }))
nodeContextMenu.append(new MenuItem({ label: 'Add New Node', click: function(event) {
    console.log('nodeContextMenu : Add New Node')
    //console.log(event)
    //console.log(document.getElementById('configuration_select').value())
    //console.log(global.sharedObject)
}}))

nodeContextMenu.append(new MenuItem({ type: 'separator' }))
nodeContextMenu.append(new MenuItem({ label: 'Delete This Node'}))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('node-context-menu', (e, params) => {
    nodeContextMenu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-node-context-menu', (event) => {
  console.log('show-node-context-menu !!!@!')
  const win = BrowserWindow.fromWebContents(event.sender)
  nodeContextMenu.popup(win)
})

// PackageGroup Context Menu
// ====================================================================
const packageGroupContextMenu = new Menu()
packageGroupContextMenu.append(new MenuItem({ label: 'PackageGroup Properties' }))
packageGroupContextMenu.append(new MenuItem({ type: 'separator' }))
//nodeContextMenu.append(new MenuItem({ label: 'Configuration Management', type: 'checkbox', checked: true }))
packageGroupContextMenu.append(new MenuItem({ label: 'Edit PackageGroup Properties'}))
packageGroupContextMenu.append(new MenuItem({ label: 'Add package to this group'}))
packageGroupContextMenu.append(new MenuItem({ type: 'separator' }))
packageGroupContextMenu.append(new MenuItem({ label: 'Add Children Package Group'}))
packageGroupContextMenu.append(new MenuItem({ label: 'Delete this Package Group'}))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('packagegroup-context-menu', (e, params) => {
    packageGroupContextMenu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-packagegroup-context-menu', (event) => {
  console.log('show-packagegroup-context-menu !!!@!')
  const win = BrowserWindow.fromWebContents(event.sender)
  packageGroupContextMenu.popup(win)
})
// Package Context Menu
// ====================================================================
const packageContextMenu = new Menu()
packageContextMenu.append(new MenuItem({ label: 'Package Properties' }))
packageContextMenu.append(new MenuItem({ type: 'separator' }))
//nodeContextMenu.append(new MenuItem({ label: 'Configuration Management', type: 'checkbox', checked: true }))
packageContextMenu.append(new MenuItem({ label: 'Edit Package Properties'}))
packageContextMenu.append(new MenuItem({ label: 'LOCK'}))
packageContextMenu.append(new MenuItem({ type: 'separator' }))
packageContextMenu.append(new MenuItem({ label: 'Delete package'}))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('package-context-menu', (e, params) => {
    packageContextMenu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-package-context-menu', (event) => {
  console.log('show-package-context-menu !!!@!')
  const win = BrowserWindow.fromWebContents(event.sender)
  packageContextMenu.popup(win)
})
