const {ipcMain, dialog} = require('electron')

ipcMain.on('open-save-configuration-dialog', (event) => {
  const options = {
    type: 'warning',
    title: 'Save Configuration',
    message: "Write current configuration to disk ?",
    buttons: ['Yes', 'No']
  }
  dialog.showMessageBox(options, (index) => {
    event.sender.send('open-save-configuration-dialog', index)
  })
})
