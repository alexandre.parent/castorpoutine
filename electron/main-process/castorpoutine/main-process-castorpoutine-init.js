const {app, ipcMain, Menu, Tray, BrowserWindow} = require('electron')
const http = require('http')
const path = require('path')
const uuidv1 = require('uuid/v1');

var fs = require('fs')

newNodeCounter = 1;

// In the main process.
global.sharedObject = {
  someProperty: 'default value',
  configurations: '',
  currentConfigurationName: '',
  currentConfigurationPointer: ''
}

ipcMain.on('read-configurations', (event) => {
    console.log('main process : configurations')
    event.sender.send('read-configurations', global.sharedObject.configurations)
})

ipcMain.on('configurationSelectBtnChange', (event, arg) => {

    console.log('main process : configurationSelectBtnChange : ' + arg)

    if(arg != ''){
        //MAIN process job is to dispatch to all that needs this event coming from a renderer.
        event.sender.send('select-configuration', arg)
    }
})

ipcMain.on('select-configuration', (event, arg) => {
    console.log('main process : select-configuration : ' + arg)
    event.sender.send('select-configuration', arg)
})
ipcMain.on('configuration-generate-archives', (event, arg) => {
    console.log('main process : configuration-generate-archives : ' + arg)
    event.sender.send('configuration-generate-archives', arg)
})

ipcMain.on('reset-configuration', (event, arg) => {
    console.log('main process : reset-configuration : ' + arg)
    event.sender.send('reset-configuration', arg)
})
ipcMain.on('configuration_select', (event, arg) => {
    console.log('main process : conf select : ' + arg)
    event.sender.send('configuration_select', arg)
})
ipcMain.on('configuration-load', (event, arg) => {
    console.log('main process : conf load : ' + arg)
    event.sender.send('configuration-load', arg)
})


ipcMain.on('configuration_unload', (event, arg) => {
    console.log('main process : conf unload : ' + arg)
    callConfigurationUnload(arg)
})
ipcMain.on('configurations_update', (event, arg) => {
    global.sharedObject.configurations = arg
})

function sendWeblaunchRequestSync(path, data, url = 'localhost', port = 8081, method= 'POST') {

    console.log('sendWeblaunchRequestSync')
    var postData = data;
    // Your Request Options
    var options = {
        host: url,
        port: port,
        path: "/load",
        method: method,
        dataType: 'json',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
        }
    };

    // The Request
    var request = http.request(options, function(response) {
        response.on('data', function(chunk) {
            if (chunk) {
                var data = chunk.toString('utf8')
                console.log(data)
            }
        });
    }).on("error", function(e) {
        // Some error handling
    });

    //optionally Timeout Handling
    request.on('socket', function(socket) {
        socket.setTimeout(5000);
        socket.on('timeout', function() {
            request.abort();
        });
    });
    request.write(postData)
    request.end()
}

function callConfigurationUnload(data = "", callback = function(){}){
    console.log('call configuration unload : ')

    var postData = JSON.stringify("");
    // Your Request Options
    var options = {
        host: "localhost",
        port: 8081,
        path: "/unload",
        method: 'POST',
        dataType: 'json',
        data: postData,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
        }
    };
    // The Request
    var request = http.request(options, function(response) {
        response.on('data', function(chunk) {
            if (chunk) {
                var data = chunk.toString('utf8')
                console.log(data)
                callback(data)
            }
        });
    }).on("error", function(e) {
        // Some error handling
    });

    //optionally Timeout Handling
    request.on('socket', function(socket) {
        socket.setTimeout(5000);
        socket.on('timeout', function() {
            request.abort();
        });
    });
    request.write(postData)
    request.end()
}
/* a.p 20191024 might be usefull later.
function callConfigurationLoad(data, callback){
    console.log('call configuration load : ' + data)
    var json = '{ "name": "'+data+'"}'
    var postData = JSON.stringify(json);
    // Your Request Options
    var options = {
        host: "localhost",
        port: 8081,
        path: "/load",
        method: 'POST',
        dataType: 'json',
        data: postData,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
        }
    };
    // The Request
    var request = http.request(options, function(response) {
        response.on('data', function(chunk) {
            if (chunk) {
                var data = chunk.toString('utf8')
                console.log(data)
                callback(data)
            }
        });
    }).on("error", function(e) {
        // Some error handling
    });

    //optionally Timeout Handling
    request.on('socket', function(socket) {
        socket.setTimeout(5000);
        socket.on('timeout', function() {
            request.abort();
        });
    });
    request.write(postData)
    request.end()

}
*/
//** initial ajax request
var querystring = '';
// You Key - Value Pairs
//var postData = querystring.stringify({
//    key: "value"
//});
var postData = '';
// Your Request Options
var options = {
    host: "localhost",
    port: 8081,
    path: "/",
    method: 'GET',
    dataType: 'json',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postData)
    }
};
/*
// The Request
var request = http.request(options, function(response) {
    response.on('data', function(chunk) {
        if (chunk) {
            console.log('the chunk')
            console.log(chunk)
            var data = chunk.toString('utf8')
            var json = JSON.parse(data);
            global.sharedObject = json;
            console.log(json)
        }
    });
}).on("error", function(e) {
    // Some error handling
});

//optionally Timeout Handling
request.on('socket', function(socket) {
    socket.setTimeout(5000);
    socket.on('timeout', function() {
        request.abort();
    });
});
request.write(postData)
request.end()
*/

ipcMain.on('save-configuration', (event, arg) => {

    console.log('save-configuration BIG RED WARNING IF THIS IS RUNNING to debug... !');
    global.sharedObject.configurations.forEach( function(entry) {

        if ( entry.segment._name == arg ) {
            console.log('Saving config named : ' + arg)
            fs.writeFile(process.env.CASTORPOUTINE_CONF_FOLDER_PATH + '/' + arg + '/' + arg + '.json', JSON.stringify(entry),(err) => {
              if (err) throw err;
              console.log('The file has been saved!');
            })
            return
        }
    })
})

ipcMain.on('configuration-state-update', (event) => {
    event.sender.send('configuration-state-update')
})

ipcMain.on('print-configuration', (event, arg) => {

    global.sharedObject.configurations.forEach( function(entry) {
        console.log(entry)
    })
})

//. Add node to configurationName
ipcMain.on('add-node', (event, arg) => {
    console.log('add-node ipc MAIN, global conf value : ')

    newNodeData = {}

    var tempName = 'NEW NODE '+ String(newNodeCounter)

    newNodeData['_syncStatus'] = false
    newNodeData['_cmInfo'] = "disk"
    newNodeData['_cmURI'] = 'cm:' +tempName +'@myconfig'
    newNodeData['_contextURI'] = tempName
    newNodeData['_install_path'] = ''
    newNodeData['_installsequence'] = ''
    newNodeData['_loadsequence'] = ''
    newNodeData['_workingFolder'] = '/castorpoutine/config/M_CONF_1'
    newNodeData['children'] = new Array(0)
    newNodeData['packageGroup'] = new Array(0)
    newNodeData['customClass'] = 'fa fa-archive'
    newNodeData['expanded'] = false
    newNodeData['fancytreeNodeType'] = 'node'
    newNodeData['folder'] = true

    newNodeData['nodeStatus'] = 'offline'
    newNodeData['title'] = tempName
    newNodeData['uuid'] = uuidv1()
    newNodeData['key'] = newNodeData['uuid']
    newNodeCounter++

    currentConfigurationPointer.segment.group.node.push(newNodeData)
    event.sender.send('selected_configuration_ui_event', currentConfigurationPointer)
})

//.System tray setup
let appIcon = null

ipcMain.on('put-in-tray', (event) => {
  //const iconName = process.platform === 'win32' ? 'windows-icon.png' : 'iconTemplate.png'
  const iconName = process.platform === 'win32' ? '256.png' : '256.png'
  const iconPath = path.join(__dirname+'../../../assets/app-icon/png', iconName)
  appIcon = new Tray(iconPath)

  const contextMenu = Menu.buildFromTemplate([
    {
    label: 'Open CastorPoutine',
    click: () => {
      event.sender.send('castorpoutine-open')
        }
    },
    {
    label: 'Start/Stop Current Configuration',
    click: () => {
      event.sender.send('castorpoutine-start-stop')
        }
    },
    {
    label: 'Open Settings',
    click: () => {
      event.sender.send('castorpoutine-settings')
        }
    },
    {
      label: 'Remove',
      click: () => {
        event.sender.send('tray-removed')
      }
    }
    ])

  appIcon.setToolTip('CastorPoutine ! INC.')
  appIcon.setContextMenu(contextMenu)
})

ipcMain.on('remove-tray', () => {
  appIcon.destroy()
})

app.on('window-all-closed', () => {
  if (appIcon) appIcon.destroy()
})


ipcMain.on('update-configuration-loadsequence', (event, arg) => {
    console.log('update-configuration ipc MAIN ')

    global.sharedObject.configurations.forEach( function(entry) {

        console.log(entry.segment._name)
        console.log('comparing conf name 1 : ' + entry.segment._name)
        console.log('comparing conf name 2 : ' + global.sharedObject.currentConfigurationName)

        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {
            console.log('Saving config name : ' + global.sharedObject.currentConfigurationName)
            console.log('load sequence value : ' + arg)
            entry.segment.group._loadsequence = arg
            //triggerLoadSequenceChangeNotification(arg)
            return
        }
    })
})

ipcMain.on('update-configuration-installsequence', (event, arg) => {
    console.log('update-configuration-installsequence ipc MAIN ')

    global.sharedObject.configurations.forEach( function(entry) {

        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {
            console.log('Saving config name : ' + global.sharedObject.currentConfigurationName)
            console.log('load sequence value : ' + arg)
            entry.segment.group._installsequence = arg
            return
        }
    })
})
ipcMain.on('update-configuration-installpath', (event, arg) => {
    console.log('update-configuration-installpath ipc MAIN ')

    global.sharedObject.configurations.forEach( function(entry) {

        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {
            console.log('Saving config name : ' + global.sharedObject.currentConfigurationName)
            console.log('load sequence value : ' + arg)
            entry.segment.group._install_path = arg
            return
        }
    })
})

ipcMain.on('delete-node', (event, arg) => {
    console.log('delete-node ipc MAIN ')
    global.sharedObject.configurations.forEach( function(entry) {

        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {

            var indexFound = 0;
            entry.segment.group.node.forEach( function(entryNode) {

                console.log('delete-node ipc MAIN ')
                console.log(entryNode)
                console.log('arg uuid to delete :  ' + arg.uuid)
                console.log('entryNode uuid to delete :  ' + entryNode.uuid)
                if (arg.uuid == entryNode.uuid) {
                    return
                }
                indexFound++
            })

            if(indexFound != entry.segment.group.node.length){

                console.log('deleting :  ' + indexFound )
                console.log(entry.segment.group.node[indexFound])
                delete entry.segment.group.node[indexFound]
                //shrink the array otherwise the .json file is corrupted with null
                //so use splice.
                entry.segment.group.node.splice(indexFound, 1)
                event.sender.send('selected_configuration_ui_event', entry)
                return
            } else {
                console.log('did not find uuid : ' + arg.uuid)
            }
        }
    })
})

ipcMain.on('update-node', (event, arg) => {
    console.log('update-node ipc MAIN ')
    //console.log(arg)

    global.sharedObject.configurations.forEach( function(entry) {

        console.log(entry.segment._name)
        console.log('comparing conf name 1 : ' + entry.segment._name)
        console.log('comparing conf name 2 : ' + global.sharedObject.currentConfigurationName)
        console.log(arg)
        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {

            console.log('updated conf node named : ' + arg._name)
            entry.segment.group.node.forEach( function(entryNode) {

                console.log('comparing node name 1 : ' + arg._name)
                console.log('comparing node name 2 : ' + entryNode._name)
                if(arg._name == entryNode._name){
                    console.log('updated node named : ' + arg._name)
                    entryNode = arg
                    event.sender.send('selected_configuration_ui_event', entry)
                    return
                }
            })
            return
        }
    })
})


ipcMain.on('select-configuration-updateselecthtml', (event, arg) => {
    console.log('select-configuration-updateselecthtml ipc MAIN ')
    event.sender.send('select-configuration-updateselecthtml', arg)
})
ipcMain.on('configuration-interval-update', (event, arg) => {
    console.log('configuration-interval-update ipc MAIN ')
    event.sender.send('configuration-interval-update', arg)
})

ipcMain.on('configuration-edited-name', (event, arg) => {
    console.log('configuration-edited-name ipc MAIN ')
    event.sender.send('configuration-edited-name', arg)
})
ipcMain.on('select-packagegroup', (event, arg) => {
    console.log('select-packagegroup ipc MAIN ')
    event.sender.send('select-packagegroup', arg)
})
ipcMain.on('select-package', (event, arg) => {
    console.log('select-package ipc MAIN ')
    event.sender.send('select-package', arg)
})
ipcMain.on('select-node', (event, arg) => {
    console.log('select-node ipc MAIN ')
    event.sender.send('select-node', arg)
    //console.log(arg)
/*
    global.sharedObject.configurations.forEach( function(entry) {

        if ( entry.segment._name == global.sharedObject.currentConfigurationName ) {

            console.log('updated conf node named : ' + arg._name)
            entry.segment.group.node.forEach( function(entryNode) {

                console.log('comparing node name 1 : ' + arg._name)
                console.log('comparing node name 2 : ' + entryNode._name)
                if(arg._name == entryNode._name){

                    console.log('updated node named : ' + arg._name)
                    global.sharedObject.currentSelectedNode = entryNode
                    return
                }
            })
        }
    })
    */
})

ipcMain.on('reset-fancytree', (event, arg) => {
    console.log('reset-fancytree ipc MAIN ')
    event.sender.send('reset-fancytree', arg)
})

ipcMain.on('update-fancytree', (event, arg) => {
    console.log('update-fancytree ipc MAIN ')
    event.sender.send('update-fancytree', arg)
})
ipcMain.on('render-configuration-table', (event, arg) => {
    console.log('render-configuration-table ipc MAIN ')
    event.sender.send('render-configuration-table', arg)
})
ipcMain.on('update-node-status', (event, arg) => {
    console.log('update-node-status ipc MAIN ')
    event.sender.send('update-node-status', arg)
})

ipcMain.on('update-configuration-propertytreeui', (event, arg) => {
    console.log('update-configuration-propertytreeui ipc MAIN ')
    event.sender.send('update-configuration-propertytreeui', arg)
})

ipcMain.on('create-configuration-legacy', (event, arg) => {
    console.log('create-configuration ipc MAIN ')

    newConfiguration = {}
    newConfiguration.uuidv1 = uuidv1()

    newSegment = {}
    newSegment.uuidv1 = uuidv1()
    newSegment._name = 'New Configuration ' + uuidv1()
    newSegment._description = 'New description'
    newSegment._masternode = 'RUNNER_1'

    newConfiguration.segment = newSegment

    newGroup = {}

    newGroup._buildSettings ='buildsettings.xml'
    newGroup._cmInfo='disk'
    newGroup._cmURI='cm:SIM@M_CONF_1'
    newGroup._contextURI='xmllayer.ant:simconfig.xml'
    newGroup._installsequence='installsequence.xml'
    newGroup._loadsequence='loadsequence.xml'
    newGroup._name='CASTORPOUTINE'
    newGroup._settings='groupsettings_lab.xml'
    newGroup._workingFolder='d:\\castorpoutine\\config\\M_CONF_1'
    newGroup.expanded = false
    newGroup.nodeStatus = 'offline'
    newGroup.fancytreeNodeType = 'node'
    newGroup.folder = true
    newGroup.customClass = 'fa fa-archive'
    newGroup.expanded = false
    newGroup.key = 'wee'
    newGroup.title = 'GROUP1'
    newGroup.children = newGroup.node
    newGroup._syncStatus = false

    newConfiguration.segment.group = newGroup

    newConfiguration.segment.group.node = new Array()


    global.sharedObject.configurations.push(newConfiguration)

    event.sender.send('read-configurations', global.sharedObject.configurations)
    event.sender.send('selected_configuration_ui_event', newConfiguration)
})


//.--- notifications setup
function triggerLoadSequenceChangeNotification(newConfName){

    const configurationNameChangeNotification = {
      title: 'Configuration Message',
      body: 'Configuration name changed to : ' + newConfName,
      icon: path.join(__dirname, '../../../assets/img/programming.png')
    }

  const myNotification = new Notification(notification.title, configurationNameChangeNotification)
    myNotification.onclick = () => {
    console.log('triggerLoadSequenceChangeNotification called')
  }
}

ipcMain.on('REST_NODE_STATUS', (event, arg) => {
    console.log('REST_NODE_STATUS ipc MAIN ')
    event.sender.send('REST_NODE_STATUS', arg)
})
