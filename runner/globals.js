// All environment variables keys
// ====================================================================

module.exports = {
  readEnvironmentVariable: function (key) {

    read(key);

  }
};

var CASTORPOUTINE_NODE   = "CASTORPOUTINE_NODE";
var CASTORPOUTINE_MASTER_NODE = "CASTORPOUTINE_MASTER_NODE";

var SIMULATION_STATE = {"unloaded":1, "loading":2, "running":3, "unloading":4};
Object.freeze(SIMULATION_STATE);

// Private methods
// ====================================================================
var read = function (key) {

    return process.env[key];
}
