@echo off
REM all env var needed to have this stuff work.
REM Script is tuned by default for git clone folder structure. Which default values should be sufficient for optimal usage.

set CASTORPOUTINE_ROOT_FOLDER=%cd%
set CASTORPOUTINE_CONF_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/conf
set CASTORPOUTINE_PKG_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/pkg
set CASTORPOUTINE_TEMP_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/temp
set CASTORPOUTINE_DAEMON_RUNNER_SRC=%CASTORPOUTINE_TEMP_FOLDER_PATH%/runner.zip
set CASTORPOUTINE_NODEJS_WIN64_MSI=%CASTORPOUTINE_ROOT_FOLDER%/3rdparty/node-v12.10.0-x64.msi
set CASTORPOUTINE_NODEJS_EL7_RPM=%CASTORPOUTINE_ROOT_FOLDER%/3rdparty/node-v10.16.3-linux-x64.tar.xz

set CASTORPOUTINE_RUNNER_NAME=%0
set CASTORPOUTINE_RUNNERIP=%1
set CASTORPOUTINE_RUNNERPORT=%2
set CASTORPOUTINE_MASTERIP=%3
set CASTORPOUTINE_MASTERPORT=%4

echo 'Launching runner with arg 0,1,2,3,4,5 %0 %1 %2 %3 %4 %5'
echo 'Current DIR : %cd%'

cd runner

call node castorpoutine.js %1 %2 %3 %4 %5
