#!/bin/bash

#kill `pidof node`

##Best pratice is to use in user folder.
export CASTORPOUTINE_ROOT_FOLDER=$PWD/runner
export CASTORPOUTINE_CONF_FOLDER_PATH=$CASTORPOUTINE_ROOT_FOLDER/conf
export CASTORPOUTINE_PKG_FOLDER_PATH=$CASTORPOUTINE_ROOT_FOLDER/pkg
export CASTORPOUTINE_TEMP_FOLDER_PATH=$CASTORPOUTINE_ROOT_FOLDER/temp
export CASTORPOUTINE_DAEMON_RUNNER_SRC=$CASTORPOUTINE_TEMP_FOLDER_PATH/runner.zip
export CASTORPOUTINE_NODEJS_WIN64_MSI=$CASTORPOUTINE_ROOT_FOLDER/3rdparty/node-v12.10.0-x64.msi
export CASTORPOUTINE_NODEJS_EL7_RPM=$CASTORPOUTINE_ROOT_FOLDER/3rdparty/node-v10.16.3-linux-x64.tar.xz

export CASTORPOUTINE_RUNNERIP=$1
export CASTORPOUTINE_RUNNERPORT=$2
export CASTORPOUTINE_MASTERIP=$3
export CASTORPOUTINE_MASTERPORT=$4
export CASTORPOUTINE_RUNNER_NAME=$5

echo 'Launching runner with arg 1,2,3,4,5 ' + $1 + ' ' + $2 + ' ' + $3 + ' ' + $4 + ' ' + $5 + ' ' + $6

cd $CASTORPOUTINE_ROOT_FOLDER
node castorpoutine.js $1 $2 $3 $4 $5 $6
