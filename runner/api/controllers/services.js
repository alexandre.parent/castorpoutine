//
var fs = require('fs')
var routes = require('./routes.js')     // include global routes mapping IE, get post url paths ...
var pidusage = require('pidusage')
var request = require('request')
//members
processes = new Array(200) //callback pointers to all processing launched by this weblaunch node
packages = new Array(1000)
configurations = new Array(100)
slaveNodes = new Array()
testVariable = 0

module.exports = class WebServices {

    //methods
    constructor() {
        console.log('Initializing CLASS : WebServices')
        //this.intervalMethod()
    }

    intervalMethod() {
        // Setup interval method logic
        // ====================================================================
        var count = 0;
        var intervalObject = setInterval(function () {
            count++;
            console.log(count, 'seconds passed');

            //1. interval job
            //1.1 validate and sync conf + pkg
            //webServices.synchronizeCurrentNode();

            var processesPid = new Array()

            processes.forEach(function(entry) {
                console.log(entry.pid)
                processesPid.push(entry.pid)
            })

            console.log(processesPid)
            pidusage(processesPid, function (err, stats) {
              console.log(stats)
            })
            //1.2 validate INTEGRITY
            /*
            if (count == 5) {
                console.log('exiting');
                clearInterval(intervalObject);
            }*/

            //this.status();
        }, 3000);
    }

    //public methods

    killProcessById(id){

        processes.forEach(function(entry) {
            if(typeof entry != 'undefined' && typeof entry.pid != 'undefined'){
                console.log('killing pid : ' + entry.pid)
                if(entry.pid == id )
                    process.kill(entry.pid, 'SIGHUP');
            }
        })
    }

    unloadCurrentNode() {
        processes.forEach(function(entry) {
            if(typeof entry != 'undefined' && typeof entry.pid != 'undefined'){
                console.log('killing pid : ' + entry.pid)
                process.kill(entry.pid, 'SIGHUP');
            }
        })
    }

    loadCurrentNode(configuration) {

        //var pathsToProcesses = new Array()

        configuration.segment.group.node.forEach(function(entry) {

            if( node._name == runnerName) {
                console.log('found pkg by name.')
            }
        })

        // program1
        const { exec } = require('child_process');

        var shellOption = {
                            cwd : "",
                            env: "",
                            encoding: "",
                            shell: "",
                            timeout: "",
                            maxBuffer: "",
                            killSignal: "",
                            uid: "",
                            gid: "",
                            windowsHide: "",
                          };

        var program1 = exec('/home/whoami/work/sui/weblaunch/src/sample/program1/firefox', (error, stdout, stderr) => {
        //var program1 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
            if (error) {
              console.error(`exec error: ${error}`);
              return;
            }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program1);

        // program2
        var program2 = exec('/home/whoami/work/sui/weblaunch/src/sample/program1/firefox', (error, stdout, stderr) => {
        //var program2 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
        return;
        }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program2);
    }

    processesStatus(){
        var jsonValue = ""//new Array(processes.length)
        //processes.forEach(function(entry){
        //    console.log(entry)
            jsonValue = JSON.stringify(processes);
        //})
        return jsonValue
    }

    synchronizeCurrentNode() {

        const { exec } = require('child_process');

        exec('rsync -rtva /home/whoami/work/sui/weblaunch/data/source/ /home/whoami/work/sui/weblaunch/data/destination', (err, stdout, stderr) => {
          if (err) {
            console.error(`exec error: ${err}`);
            return;
          }

          console.log(`Number of files ${stdout}`);
        });
    }

    configurationsRead(returnValue){

        var status;
        //-- Read configurations
        console.log('Entering status');
        console.log('Before status read configurations.json : base path : ' + __dirname);
/*
        return fs.readFileSync( __dirname + '/models/configurations.json', 'utf8',
            function (err, data) {
                if (err) throw err;
                    returnValue = JSON.stringify(data)
                    return returnValue;
            });*/
    }

    configurationRead(configurationName){

        var fileBuffer;
        //-- Read configurations
        console.log('Entering configurationSelect');
        console.log('Conf name ' + configurationName);

        return fs.readFileSync( __dirname + '/models/configurations/' + configurationName + '.json', 'utf8',
            function (err, data) {
                if (err) throw err;
                    fileBuffer = JSON.stringify(data)
                    return fileBuffer;
            });
    }

    configurationCopy(configurationName, newConfigurationName){

        var fileBuffer;
        //-- Read configurations
        console.log('Entering configurationSelect');
        console.log('Conf name ' + configurationName);

        fs.copyFile(__dirname + '/models/configurations/' + configurationName + '.json', __dirname + '/models/configurations/' + newConfigurationName + '.json', (err) => {
          if (err) throw err;
          console.log('source.txt was copied to destination.txt');
        });
    }

    configurationCreate(configurationFrom , configurationTo){

        var fileBuffer;
        //-- Read configurations
        console.log('Entering configurationCreate');
        console.log('Conf from ' + configurationFrom);
        console.log('Conf to ' + configurationTo);

        var pathFrom='', pathTo=''
        pathFrom = __dirname + '/models/configurations/' + configurationFrom + '.json'
        pathTo = __dirname + '/models/configurations/' + configurationTo + '.json'

        if(configurationFrom == ''){
            pathFrom = __dirname + '/models/configuration_template.json'
        }

        fs.copyFile(pathFrom, pathTo, (err) => {
          if (err) throw err;
          console.log('source.txt was copied to destination.txt');
        });
    }

    configurationDelete(configurationName){

        var fileBuffer;
        //-- Read configurations
        console.log('Entering configurationSelect');
        console.log('Conf name ' + configurationName);
        fs.exists(__dirname + '/models/configurations/'+configurationName+'.json', function(exists) {
          if(exists) {
            //Show in green
            console.log('File exists. Deleting now ...');
            fs.unlink(__dirname + '/models/configurations/'+configurationName+'.json');
          } else {
            //Show in red
            console.log('File not found, so not deleting.');
          }
        });
    }

    //
    //------------
    startMasterNodeIntervalFunction(){
        // Setup interval method logic
        // ====================================================================
        var count = 0;
        var intervalObject = setInterval(function () {
            count++;
            console.log(count, 'Master Interval Function');

            if( typeof slaveNodes != undefined ) {
                console.log('Master has nb slaves : ' + slaveNodes.length);

                slaveNodes.forEach(function(entry) {
                    //if(typeof entry != undefined)
                        console.log(entry)
                })
            }
        }, 2000)
    }

    getNodesStatusJSON(){
        return JSON.stringify(slaveNodes)
    }

    handleSlaveNodeAcknowledge(slaveNodeJson) {

        console.log('handleSlaveNodeAcknowledge')
        //console.log(slaveNodeJson)
        var slaveNodeJsonParsedEntry = JSON.parse(slaveNodeJson)
        var found = false

        slaveNodes.forEach(function(entry) {
            //console.log('testing entry : ' + entry.name)
            //console.log('testing slaveNodeJsonParsedEntry : ' + slaveNodeJsonParsedEntry.name)
            if( entry.name == slaveNodeJsonParsedEntry.name ) {
                console.log('Node Compare OK : ' + entry.name + ' , ' + slaveNodeJsonParsedEntry.name)
                entry = slaveNodeJsonParsedEntry
                found = true
            }
        })

        if(!found){
            console.log('adding slave node :')
            //console.log(slaveNodeJsonParsedEntry)
            slaveNodes.push(slaveNodeJsonParsedEntry)
        }
    }

    startSlaveNodeIntervalFunction(){

        console.log('global access test')
        //console.log(weblaunchConfiguration)

        // Setup interval method logic
        // ====================================================================
        var count = 0;
        var intervalObject = setInterval(function () {

            var request = require('request')
            const http = require('http')

            count++
            console.log(count, 'Slave Interval Function')
            //console.log(weblaunchConfiguration)

            var request = request.post({
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                url: 'http://localhost:8081/acknowledge',
                body: JSON.stringify(weblaunchConfiguration)
            }, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log(body)
                }
            })

        }, 3000);
    }


    //-- Test stuff
    testVariable(){
        console.log('service.js testVariable : ' + testVariable)
        testVariable++
    }
}
