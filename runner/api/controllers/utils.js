//
var fs = require('fs')

var pidusage = require('pidusage')
var request = require('request')

//members
processes = new Array(200) //callback pointers to all processing launched by this weblaunch node
packages = new Array(1000)
configurations = new Array(100)
slaveNodes = new Array()
testVariable = 0

classname = 'SlaveNode'
intervalMethodDelayInMs = 3000

// Utils
// All functions executed on a slave node.
// ====================================================================
module.exports = class Utils {

    //methods
    constructor() {
        console.log('Initializing CLASS : SlaveNode')
        //this.intervalMethod()
    }

    //public methods

    killProcessById(id){
        /*
        processes.forEach(function(entry) {
            if(typeof entry != 'undefined' && typeof entry.pid != 'undefined'){
                console.log('killing pid : ' + entry.pid)
                if(entry.pid == id )
                    process.kill(entry.pid, 'SIGHUP');
            }
        })
    }
}
