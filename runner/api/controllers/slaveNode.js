//slaveNode.js
//1. features
//1.1 receive a configuration json from master node and store it.
//1.2
//2. Synchronize configuration packages
//2.1 Use RSYNC
//2.2 use SSH
//2.3 Use NSF
//3. Load a given configuration.
//3.1 Store and give access to process details.
//4. Unload a given configuration



var fs = require('fs')

var pidusage = require('pidusage')
var request = require('request')

//members
processes = new Array(200) //callback pointers to all processing launched by this weblaunch node
packages = new Array(1000)
configurations = new Array(100)
slaveNodes = new Array()
testVariable = 0

classname = 'SlaveNode'
intervalMethodDelayInMs = 3000
state = 1 // 1 = standby, 2 = conf selected, 3 = synching, 4 = synched, 5 = loading, 6 = running, 7 = unloading

currentConfiguration = ''
// SlaveNode
// All functions executed on a slave node.
// ====================================================================
module.exports = class SlaveNode {

    constructor() {
        console.log('Initializing CLASS : SlaveNode')
        //this.intervalMethod()
    }

    // startExpressRestRouting
    // HTTP rest routes setup.
    // ====================================================================
    startExpressRestRouting(app, configurationFile){
        app.use( bodyParser.json() )
    }

    unloadCurrentNode() {
        processes.forEach(function(entry) {
            if(typeof entry != 'undefined' && typeof entry.pid != 'undefined'){
                console.log('killing pid : ' + entry.pid)
                process.kill(entry.pid, 'SIGHUP');
            }
        })
    }

    loadCurrentNode() {

        // program1
        const { exec } = require('child_process');

        var shellOption = {
                            cwd : "",
                            env: "",
                            encoding: "",
                            shell: "",
                            timeout: "",
                            maxBuffer: "",
                            killSignal: "",
                            uid: "",
                            gid: "",
                            windowsHide: "",
                          };

        var program1 = exec('/home/whoami/work/sui/weblaunch/src/sample/program1/firefox', (error, stdout, stderr) => {
        //var program1 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
            if (error) {
              console.error(`exec error: ${error}`);
              return;
            }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program1);

        // program2
        var program2 = exec('/home/whoami/work/sui/weblaunch/src/sample/program1/firefox', (error, stdout, stderr) => {
        //var program2 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
        return;
        }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program2);
    }

    processesStatus(){
        var jsonValue = ""//new Array(processes.length)
        //processes.forEach(function(entry){
        //    console.log(entry)
            jsonValue = JSON.stringify(processes);
        //})
        return jsonValue
    }

    synchronizeCurrentNode() {

        const { exec } = require('child_process');

        exec('rsync -rtva /home/whoami/work/sui/weblaunch/data/source/ /home/whoami/work/sui/weblaunch/data/destination', (err, stdout, stderr) => {
          if (err) {
            console.error(`exec error: ${err}`);
            return;
          }

          console.log(`Number of files ${stdout}`);
        });
    }

    getNodesStatusJSON() {
        return JSON.stringify(slaveNodes)
    }

    handleSlaveNodeAcknowledge(slaveNodeJson) {

        console.log('handleSlaveNodeAcknowledge')
        //console.log(slaveNodeJson)
        var slaveNodeJsonParsedEntry = JSON.parse(slaveNodeJson)
        var found = false

        slaveNodes.forEach(function(entry) {
            //console.log('testing entry : ' + entry.name)
            //console.log('testing slaveNodeJsonParsedEntry : ' + slaveNodeJsonParsedEntry.name)
            if( entry.name == slaveNodeJsonParsedEntry.name ) {
                console.log('Node Compare OK : ' + entry.name + ' , ' + slaveNodeJsonParsedEntry.name)
                entry = slaveNodeJsonParsedEntry
                found = true
            }
        })

        if(!found){
            console.log('adding slave node :')
            //console.log(slaveNodeJsonParsedEntry)
            slaveNodes.push(slaveNodeJsonParsedEntry)
        }
    }

    // Setup interval method logic
    // ====================================================================
    startSlaveNodeIntervalFunction(configurationFile){

        var count = 0

        var intervalObject = setInterval(function () {

            count++
            console.log(count, 'Slave Interval Function - state : ' + state)

            switch (state) {

                case 1:
                    idle(configurationFile)
                    break;
                case 2:
                    confselected()
                    break;
                case 3:
                    synching()
                    break;
                case 4:
                    synched()
                    break;
                case 5:
                    loading()
                    break;
                case 6:
                    running()
                    break;
                case 7:
                    unloading()
                    break;
              default:
                break
            }
        }, intervalMethodDelayInMs)
    }
}

//utility
function getStateStringValue(){
    switch (state) {

        case 1:
            return 'idle'
            break;
        case 2:
            return 'confselected'
            break;
        case 3:
            return 'synching'
            break;
        case 4:
            return 'Synched'
            break;
        case 5:
            return 'loading'
            break;
        case 6:
            return 'running'
            break;
        case 7:
            return 'unloading'
            break;
      default:
        break
    }
}

// confselected
// executed when beeing told to switch config
// ====================================================================
function confselected(){}

 // idle
 // executed when beeing told synch files
 // ====================================================================
 function idle(configurationFile){
     console.log('- idle function -' )
     var request = require('request')
/*
     //1. send node status to master.
     var request = request.post({
         headers: {'content-type' : 'application/x-www-form-urlencoded'},
         url: 'http://localhost:8080/node/status',
         body: JSON.stringify(configurationFile)
     }, function (error, response, body) {
         if (!error && response.statusCode == 200) {
             console.log(body)
         }
     })
     var request = require('request');
     //var body = JSON.stringify(configurationFile)
     var body = configurationFile
     request.post({url:'http://localhost:8080/node/status', form: {name:"node_Status=testnode"}} ,function (error, response, body) {
       console.log('error:', error); // Print the error if one occurred
       console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
       console.log('body:', body); // Print the HTML for the Google homepage.
     })
*/

     const formData = {
       client_id:     '0123456789abcdef',
       client_secret: 'secret',
       code:          'abcdef',
       name:          'testnode',
       state:         getStateStringValue(),
       status:        'ONLINE'
    }

    request.post(
      {
        url: 'http://' + configurationFile.master + ':8081/node/status',
        json: formData
      },
      function (err, httpResponse, body) {
            console.log(err, body);
            currentConfiguration = body
            console.log('error:', err); // Print the error if one occurred
            console.log('statusCode:', httpResponse && httpResponse.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
      }
    )
}

// synching
// executed when beeing told synch files
// ====================================================================
 function synching(){}

// synched
// executed when beeing conf selected and files synch
// ====================================================================
function synched(){}

// loading
// executed when starting configuration processes.
// ====================================================================
function loading(){}

// running
// executed when configuration is running.
// ====================================================================
function running(){}

// unloading
// executed when unloading the configuration processes
// ====================================================================
function unloading() {}
