//masterNode.js
//1. features
//1.1 receive a configuration json from master node and store it.
//1.2
//2. Synchronize configuration packages
//2.1 Use RSYNC
//2.2 use SSH
//2.3 Use NSF
//3. Load a given configuration.
//3.1 Store and give access to process details.
//4. Unload a given configuration


//
var fs = require('fs')

var pidusage = require('pidusage')
var request = require('request')

//members
processes = new Array(200) //callback pointers to all processing launched by this weblaunch node
packages = new Array(1000)
currentConfiguration = "";

//slaveNodes = new Array()startMasterNodeIntervalFunction
testVariable = 0

classname = 'MasterNode'
intervalMethodDelayInMs = 3000

state = 1 // 0 = standby, 1 = conf selected, 2 = synching, 3 = synched, 4 = loading, 5 = running, 6 = unloading
var masterServerIp   = ''
var masterServerPort = ''
var runnerIp= ''
var runnerPort= ''


// SlaveNode
// All functions executed on a slave node.
// ====================================================================
module.exports = class MasterNode {

    constructor() {
        console.log('Initializing CLASS : MasterNode')
        //this.intervalMethod()
    }

    // startExpressRestRouting
    // HTTP rest routes setup.
    // ====================================================================
    startExpressRestRouting(app, argrunnerName, argRunnerIp, argRunnerPort, argMasterIp, argMasterPort){

        runnerName          = argrunnerName
        runnerIp            = argRunnerIp
        runnerPort          = argRunnerPort
        masterServerIp      = argMasterIp
        masterServerPort    = argMasterPort

        app.use( bodyParser.json() )

        app.get('/', function(req, res) {
            //res.sendfile('index.html', { root: __dirname + "/../ui" } );
            //console.log('path : /');
            //console.log('configurations : ');//JSON.stringify(configurations)

            var returnValue = "<html>";
            returnValue += "Welcome to CastorPoutine Runner API Menu.";
            returnValue += "<br>";
            returnValue += "Runner information";
            returnValue += "<br><br>";
            returnValue += "Name : " + runnerName + "<br>";
            returnValue += "Runner IP : " + runnerIp + "<br>";
            returnValue += "Runner Port : " + runnerPort + "<br>";
            returnValue += "Master IP : " + masterServerIp + "<br>";
            returnValue += "Master Port : " + masterServerPort + "<br>";
            returnValue += "<br>";
            returnValue += "State : " + state + ' - ' + getStateStringValue(); + "<br>";
            returnValue += "<br><br>";
            returnValue += "Current Configuration : ";
            returnValue += "<br>" + JSON.stringify(currentConfiguration);

            returnValue += "</html>";

            //returnValue = webServices.configurationsRead();

            console.log('returnValue : ' + returnValue);
            res.end( returnValue );
        });

        app.post('/configuration/select', function (req, res) {
            console.log('/configuration/select')
            console.log(req.body);      // your JSON
            //console.log(req)

            //var jsonRequest = JSON.parse(req.body)
            var jsonRequest = req.body
            var jsonResponse = {}

            console.log('Conf select name : ' + jsonRequest.name)
            currentConfiguration = jsonRequest

            if(jsonRequest != '')
                state = 1
            res.end( 'ok ...' )
        })

        app.post('/status', function (req, res) {
            console.log('/status')
            res.end( 'OK' )
        })


        app.post('/configuration/load', function (req, res) {

            var jsonString = '';

            if (req.method == 'POST') {

                req.on('data', function (data) {
                    jsonString += data;
                    console.log(data);
                });

                req.on('end', function () {
                    //jsonString = JSON.parse(jsonString)
                });
            }


            console.log('/configuration/load');
            console.log(jsonString);
            console.log('request body : ' + req.body)
            console.log(req.body)
            //var bodyParsed = JSON.parse(req.body)
            //console.log('request body : ' + bodyParsed)

            currentConfiguration = req.body

            //var returnValue = "Loading conf : " + currentConfiguration.segment._name;
            //returnValue = webServices.loadCurrentNode(jsonString)

            res.end( 'do siomething.' );
        })
    }

    unloadCurrentNode() {
        processes.forEach(function(entry) {
            if(typeof entry != 'undefined' && typeof entry.pid != 'undefined'){
                console.log('killing pid : ' + entry.pid)
                process.kill(entry.pid, 'SIGHUP');
            }
        })
    }

    loadCurrentNode() {

        // program1
        const { exec } = require('child_process');

        var shellOption = {
                            cwd : "",
                            env: "",
                            encoding: "",
                            shell: "",
                            timeout: "",
                            maxBuffer: "",
                            killSignal: "",
                            uid: "",
                            gid: "",
                            windowsHide: "",
                          };

        var program1 = exec('firefox', (error, stdout, stderr) => {
        //var program1 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
            if (error) {
              console.error(`exec error: ${error}`);
              return;
            }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program1);

        // program2
        var program2 = exec('calc', (error, stdout, stderr) => {
        //var program2 = exec('/home/whoami/work/sui/weblaunch/src/sample/program2/deluge', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
        return;
        }
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });
        processes.push(program2);
    }

    processesStatus(){
        var jsonValue = ""//new Array(processes.length)
        //processes.forEach(function(entry){
        //    console.log(entry)
            jsonValue = JSON.stringify(processes);
        //})
        return jsonValue
    }

    synchronizeCurrentNode() {

        const { exec } = require('child_process');

        exec('rsync -rtva /home/whoami/work/sui/weblaunch/data/source/ /home/whoami/work/sui/weblaunch/data/destination', (err, stdout, stderr) => {
          if (err) {
            console.error(`exec error: ${err}`);
            return;
          }

          console.log(`Number of files ${stdout}`);
        });
    }

    //
    //------------
    startMasterNodeIntervalFunction(){

        // Setup interval method logic
        // ====================================================================
        var count = 0;

        var intervalObject = setInterval(function () {

            count++
            console.log(count, 'Node : Interval Function - state : ' + state)

            switch (state) {

                case 1:
                    idle()
                    break;
                case 2:
                    confselected()
                    break;
                case 3:
                    synching()
                    break;
                case 4:
                    synched()
                    break;
                case 5:
                    loading()
                    break;
                case 6:
                    running()
                    break;
                case 7:
                    unloading()
                    break;
              default:
                break
            }
        }, intervalMethodDelayInMs)

    }

    getNodesStatusJSON(){
        return JSON.stringify(slaveNodes)
    }

    handleSlaveNodeAcknowledge(slaveNodeJson) {

        console.log('handleSlaveNodeAcknowledge')
        //console.log(slaveNodeJson)
        var slaveNodeJsonParsedEntry = JSON.parse(slaveNodeJson)
        var found = false

        slaveNodes.forEach(function(entry) {
            //console.log('testing entry : ' + entry.name)
            //console.log('testing slaveNodeJsonParsedEntry : ' + slaveNodeJsonParsedEntry.name)
            if( entry.name == slaveNodeJsonParsedEntry.name ) {
                console.log('Node Compare OK : ' + entry.name + ' , ' + slaveNodeJsonParsedEntry.name)
                entry = slaveNodeJsonParsedEntry
                found = true
            }
        })

        if(!found){
            console.log('adding slave node :')
            //console.log(slaveNodeJsonParsedEntry)
            slaveNodes.push(slaveNodeJsonParsedEntry)
        }
    }
}

//utility
function getStateStringValue(){
    switch (state) {

        case 1:
            return 'idle'
            break;
        case 2:
            return 'confselected'
            break;
        case 3:
            return 'synching'
            break;
        case 4:
            return 'Synched'
            break;
        case 5:
            return 'loading'
            break;
        case 6:
            return 'running'
            break;
        case 7:
            return 'unloading'
            break;
      default:
        break
    }
}

// confselected
// executed when beeing told to switch config
// ====================================================================
function confselected(){ console.log('- confselected function -' ) }

 // idle
 // executed when beeing told nothing
 // ====================================================================
 function idle(){
     console.log('- idle function - masterServerIp masterServerPort ' + masterServerIp + ' ' + masterServerPort )

     const formData = {
       client_id:     '0123456789abcdef',
       client_secret: 'secret',
       code:          'abcdef',
       ip:            runnerIp,
       port:          runnerPort,
       name:          runnerName,
       state:         getStateStringValue(),
       status:        'HTTP : ONLINE '
    }

    request.post({
        url: 'http://' + masterServerIp + ':' + masterServerPort + '/node/status',
        json: formData
      },
      function (err, httpResponse, body) {
            console.log(err, body);
            currentConfiguration = body
            console.log('error:', err); // Print the error if one occurred
            console.log('statusCode:', httpResponse && httpResponse.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
      }
    )
}

// synching
// executed when beeing told synch files
// ====================================================================
 function synching(){}

// synched
// executed when beeing conf selected and files synch
// ====================================================================
function synched(){}

// loading
// executed when starting configuration processes.
// ====================================================================
function loading(){}

// running
// executed when configuration is running.
// ====================================================================
function running(){}

// unloading
// executed when unloading the configuration processes
// ====================================================================
function unloading() {}
