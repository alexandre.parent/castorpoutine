//routes.js
//1. features
//1.1 map url to specific method calls for futher usage
// ====================================================================
//var express = require('express');

//require('./services.js');    // include global services methods
var WebServices = require('./services.js');    // include global services methods
var webServices = new WebServices();
bodyParser = require('body-parser');

module.exports = {
    initialize: function ( app ) {
        initialize(app);
    }
}

// Private methods
// ====================================================================
var initialize = function ( app ) {

    app.use( bodyParser.json() )

    app.get('/', function(req, res) {
        //res.sendfile('index.html', { root: __dirname + "/../ui" } );
        console.log('path : /');
        console.log('configurations : ');//JSON.stringify(configurations)

        var returnValue = "Welcome to CastorPoutine Runner API Menu.";
        //returnValue = webServices.configurationsRead();

        console.log('returnValue : ' + returnValue);
        res.end( returnValue );
    });

    app.get('/api', function (req, res) {
        res.end( "status --- Not implemented" );
    });

    app.get('/configuration/select', function (req, res) {
        var returnValue = "";//M_NZ_UPD
        var confName = req.query.configurationName
        console.log('Conf name :')
        console.log(confName)

        //returnValue = webServices.configurationRead(confName);
        res.end( returnValue );
    });

    app.post('/configuration/select', function (req, res) {
        console.log('/configuration/select')
         console.log(req.body);      // your JSON
        //console.log(req)

        //var jsonRequest = JSON.parse(req.body)
        var jsonRequest = req.body
        var jsonResponse = {}

        console.log(' conf select name : ' + jsonRequest.name)
        var returnValue = ""
        returnValue = webServices.configurationRead(jsonRequest.name)
        console.log(returnValue)

        res.end( returnValue )
    });

    app.post('/configuration/create', function (req, res) {
        console.log('/configuration/create');
        returnValue = webServices.configurationCreate(req.query.configurationFrom, req.query.configurationName);
        res.end( returnValue );
    });
    app.post('/configuration/delete', function (req, res) {
        console.log('/configuration/delete');
        returnValue = webServices.configurationDelete(req.query.configurationName);
        res.end( returnValue );
    });
    app.post('/configuration/rename', function (req, res) {
        console.log('/configuration/rename');
        req.query.configurationNameFrom;
        req.query.configurationNameTo;
        returnValue = webServices.configurationRename(returnValue);
        res.end( returnValue );
    });

    app.get('/processes', function (req, res) {
        console.log('/processes');
        var returnValue = "";
        returnValue = webServices.processesStatus()
        res.end( returnValue );
    });
    app.post('/processes', function (req, res) {
        console.log('/processes');
        var returnValue = "";
        returnValue = webServices.processesStatus()
        res.end( returnValue );
    });

    app.post('/configuration/load', function (req, res) {

        var jsonString = '';

        if (req.method == 'POST') {

            req.on('data', function (data) {
                jsonString += data;
            });

            req.on('end', function () {
                console.log(JSON.parse(jsonString));
            });
        }

        console.log('/configuration/load');
        console.log(jsonString);
        var returnValue = "";

        returnValue = webServices.loadCurrentNode(jsonString)

        res.end( returnValue );
    });

    app.post('/acknowledge', function (req, res) {

        console.log('/acknowledge')

        if (req.method == 'POST') {
            var jsonString = ''

            req.on('data', function (data) {
                jsonString += data;
                //console.log('/acknowledge value : ' + jsonString)
            })

            req.on('end', function () {

                //console.log('end jsonstring :')
                //console.log(jsonString)
                //console.log(req.body)
                if(typeof jsonString != undefined && jsonString != ''){
                    //console.log(JSON.parse(jsonString));
                    //console.log('/acknowledge DATA')
                    //console.log(jsonString)
                    webServices.handleSlaveNodeAcknowledge(jsonString)
                }
                res.end( 'Received Acknowledge' )
            })
        }
    })

    app.get('/sync', function (req, res) {
        //res.end( "'index.html', { root: __dirname + "/../ui" } " );
    })

    app.get('/unload', function (req, res) {
        console.log('/unload');
        var returnValue = "";
        returnValue = webServices.unloadCurrentNode()
        res.end( returnValue );
    })

    app.post('/unload', function (req, res) {
        console.log('/unload');
        var returnValue = "";
        returnValue = webServices.unloadCurrentNode()
        res.end( returnValue );
    })

    app.get('/nodestatus', function (req, res) {
        console.log('/nodestatus')
        var returnValue = ""
        returnValue = webServices.getNodesStatusJSON()
        res.end( returnValue )
    })

    app.post('/nodestatus', function (req, res) {
        console.log('/nodestatus')
        var returnValue = ""
        returnValue = webServices.getNodesStatusJSON()
        res.end( returnValue )
    })

    app.get('/updateEnvironmentVariables', function (req, res) {
        res.end( "status --- Not implemented" );
    });

    // manual test
    app.get('/testvariable', function (req, res) {
        console.log('/testvariable');
        var returnValue = "";

        returnValue = webServices.testVariable()
        res.end( returnValue );
    });

    //function isInt(value) {
    //  return !isNaN(value) &&
    //         parseInt(Number(value)) == value &&
    //         !isNaN(parseInt(value, 10));
    //}
}
