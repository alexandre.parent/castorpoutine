// Nodejs imports
// =======================================================================
var express = require('express')
var app = express()
var fs = require('fs')

// Custom import
// =======================================================================
var globals = require('./globals.js')    // include global variables
var routes = require('./api/controllers/routes.js')

var MasterNode = require( './api/controllers/masterNode.js')    // include global services methods
var masterNode = new MasterNode()

// Main and web main
// ========================================================================
var myArgs = process.argv.slice(2);
var runnerIp='',runnerPort='',masterIp='',masterPort=''

runnerName  = process.argv[2]
runnerIp    = process.argv[3]
runnerPort  = process.argv[4]
masterIp    = process.argv[5]
masterPort  = process.argv[6]

if(runnerName == '' || runnerIp == '' || runnerPort == '') {
    console.log("ERROR : Cannot launch withou argument 0 runnerName 1 ip, 2 port http://%s:%s", runnerIp, runnerPort)
    return
}

console.log("Launching runner with ip, port http://%s:%s master : %s %s ", runnerIp, runnerPort, masterIp, masterPort)
var formedURL = runnerIp + ":" + runnerPort

var server = app.listen(runnerPort, function () {

    // Parameters
    // ====================================================================
    var host = server.address().address
    var port = server.address().port

    console.log("castorpoutine - listening at http://%s:%s", host, port)

    // Route app.
    // ====================================================================
    //routes.initialize(app)

    // Service communications
    // ====================================================================
    masterNode.startExpressRestRouting(app, runnerName, runnerIp, runnerPort, masterIp, masterPort)
    masterNode.startMasterNodeIntervalFunction()

})
