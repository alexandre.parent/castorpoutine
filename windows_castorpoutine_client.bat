# all env var needed to have this stuff work.
# Script is tuned by default for git clone folder structure. Which default values should be sufficient for optimal usage.

set CASTORPOUTINE_ROOT_FOLDER=%cd%
set CASTORPOUTINE_CONF_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/conf
set CASTORPOUTINE_PKG_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/pkg
set CASTORPOUTINE_TEMP_FOLDER_PATH=%CASTORPOUTINE_ROOT_FOLDER%/temp
set CASTORPOUTINE_DAEMON_RUNNER_SRC=%CASTORPOUTINE_TEMP_FOLDER_PATH%/runner.zip
set CASTORPOUTINE_NODEJS_WIN64_MSI=%CASTORPOUTINE_ROOT_FOLDER%/3rdparty/node-v12.10.0-x64.msi
set CASTORPOUTINE_NODEJS_EL7_RPM=%CASTORPOUTINE_ROOT_FOLDER%/3rdparty/node-v10.16.3-linux-x64.tar.xz

for /F "tokens=14" %i in ('"ipconfig | findstr IPv4"') do SET CASTORPOUTINE_IP=%i

echo '--- Launching CastorPoutine Client v0x01111 ---'
echo '--- Runtime ENVIRONMENT Variables ...       ---'

echo 'CASTORPOUTINE_ROOT_FOLDER=' + %CASTORPOUTINE_ROOT_FOLDER%
echo 'CASTORPOUTINE_CONF_FOLDER_PATH=' + %CASTORPOUTINE_CONF_FOLDER_NAME%
echo 'CASTORPOUTINE_PKG_FOLDER_PATH=' + %CASTORPOUTINE_PKG_FOLDER_PATH%
echo 'CASTORPOUTINE_TEMP_FOLDER_PATH=' + %CASTORPOUTINE_TEMP_FOLDER_PATH%
echo 'CASTORPOUTINE_DAEMON_RUNNER_SRC=' + %CASTORPOUTINE_DAEMON_RUNNER_SRC%
echo 'CASTORPOUTINE_NODEJS_WIN64_MSI=' + %CASTORPOUTINE_NODEJS_WIN64_MSI%
echo 'CASTORPOUTINE_NODEJS_EL7_RPM=' + %CASTORPOUTINE_NODEJS_EL7_RPM%
echo 'CASTORPOUTINE_IP=' + %CASTORPOUTINE_IP%

echo 'Creating archive for clients'
powershell Compress-Archive -Path .\runner\ -Update -DestinationPath runner.zip -CompressionLevel Fastest

cd electron
npm start
